---
title: "Construire un site Web dynamique avec R et Shinydashboard"
author: "Cédric TASSART"
date: "2021-10-11"
output:
  ioslides_presentation:
    fig_caption: yes
    highlight: zenburn
    keep_md: yes
    smaller: yes
    widescreen: yes
  beamer_presentation:
    highlight: zenburn
editor_options:
  chunk_output_type: inline
---


# **Plan de la formation**

1. Introduction
2. Présentation rapide de Shiny
3. Un exemple pas à pas
4. Interaction entre l’interface et le programme
5. Gestion de la mise en page d’une application
6. Les fonctions du server
7. shinyfiles et interfaces dynamiques
8. Bon à savoir

# Introduction

Supports associés à la formation nationale *Concevoir une application Web dynamique avec R et shinydashboard*.  
Le code source est disponible [ici](https://gitlab.com/CTassart/formation-rshiny-bookdown).

## Préparation de son environnement de travail


- **AUS V3** - la collection `Formation`.  

  - `R`,  
  - `RStudio`,  
  - `V:\SAUVEGARDES\_Shiny_Formation HdF`.  

- **SSP Cloud**  

  - service `RStudio`,  

---

- Les supports de la formation *Concevoir une application Web dynamique avec R et shinydashboard* c'est à dire :  

  - Code à tester,  
  - corrigés exercices    
  
- les données :  
  
    - `poplegale_6815.sas7bdat`,  
    - `poplegale_6815.rds`,  
    - `projections.xls`,
    - `messages.ods`.  
  
---

**Ressources**

Bien que largement remaniée, cette formation a été élaborée à partir de la formation dispensée par les formateurs de la Direction Régionale de l'Institut National de la Statistique et des Etudes Economiques des Hauts-de-France.

<!--chapter:end:index.Rmd-->

# **Plan de la formation**

1. Introduction
2. Présentation rapide de Shiny
3. Un exemple pas à pas
4. Interaction entre l’interface et le programme
5. Gestion de la mise en page d’une application
6. Les fonctions du server
7. shinyfiles et interfaces dynamiques
8. Bon à savoir

# **Présentation rapide de Shiny**

## Shiny 

Shiny est un package R qui permet la création d'applications Web interactives.


## Principe du client-serveur

Les utilisateurs manipulent l'interface.
Le serveur actualise l'affichage des résultats sur l'interface via du code R.

![](img/12_client_serveur.png){width="750"}


## Structure d’une application Shiny

Une application Shiny est composée de deux éléments :

* **ui **  
la description de l'interface utilisateur (i.e. l'apparence de l'application)

* **server**  
les instructions R nécessaires aux traitements et calculs.  
<br>

Ces deux éléments peuvent simplement être répartis dans :

* un fichier (dit fichier unique) nommé `app.R`, 
* deux fichiers (dits fichiers multiples) nommés `ui.R` et `server.R`.


## Création d’une application à partir de Rstudio

- File  
  *- New Project...*  
  *- New directory*  
- Shiny web application...  

![](img/13 Creer appli.png){width="750"}

---

![](img/14 Creer appli.png){width="750"}

## Lancement d’une application à partir de Rstudio

![](img/15 lancer appli.png){width="550"}

---

### **Exercice 1**

<!-- ![](img/16_Exo1.png){width="750"} -->

Dans RStudio, **créer une nouvelle application Shiny**

- `Application name`	: «Exo_1»,
- `Application type`	: multiple file (ui.r/server.r),
- `directory`	: votre répertoire «PGM».

**Lancer l’application Shiny ainsi créée**


<!--chapter:end:01-Presentation_rapide.Rmd-->

# **Un exemple pas à pas**

---

Squelette d'une application shiny en 1 fichier

![](img/20 squelette appli.png){width="750"}




## Une application vide

Chargement en dehors de l’application :  
- des librairies 


```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)
```

---

- des données (base des populations)  



```r
### Traitement dans l'environnement global, en dehors de l'application
# Chargement et traitement sur la table des populations de 1968 à 2015  

pop<- haven::read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
  rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee <- c("Pop_2015", "Pop_2010", "Pop_2006" ,"Pop_1999", "Pop_1990", "Pop_1982", "Pop_1975", "Pop_1968")
```

---

- des parties interface et server

```r
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
)

#serveur de calculs
server <- function(input, output) {
}

# Run the application 
shinyApp(ui = ui, server = server)
```

---

**A ce stade l'application est vide**

![](img/21 Apppli vide.png){width="750"}

(*Le code est disponible sur le `bookdown`*)


## Implémentation de l'`UI`


```r
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une année",
              choices=choix_annee)
  )
```

- Ajout d’un titre et d’une liste déroulante dans l’interface (UI).

- On remarque que les choix de la liste sont paramétrés à partir d’une variable
(choix_annee) définie en dehors de l’application.

---

- **À ce stade, il n’ y a pas de calculs dans le server**

![](img/22 Apppli vide.png){width="750"}

(*Le code est disponible sur le `bookdown`*)


## Traitement des données


```r
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderTable({
    pop %>% group_by(REGION) %>%
      summarise_at(input$annee,sum,na.rm=T)
  })
}
# Run the application 
shinyApp(ui = ui, server = server)
```

- Construction d’un `tableau` de la population par région dans le server.

---

- **A ce stade, il n’est pas affiché dans l’interface de l’application**

![](img/22 Apppli vide.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

## Retour au `server`


```r
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une annee",
              choices=choix_annee)
  ,tableOutput("tableau")
)
```

* Un titre
* une liste déroulante
* et un tableau modifiable par des sélections

---

![](img/23 Apppli vide.png){width="350"}

**Nous avons une application réactive et pouvons afficher les données de :** `tableau`

(*Le code est disponible sur le `bookdown`*)

---

**L'objet input$annee est automatiquement modifié :**

![](img/25 Pas A Pas.png){width="750"}

---

### **Exercice 2**

**Modifier l’application fournie (EXO2_original) en modifiant titres, couleur**

***A partir du code de l'application fourni ci-dessous, modifier les éléments suivants :***

- titre : "Exo_2",
- titre du graphique : "Histogramme de valeurs aléatoires suivant une loi normale",
- valeur minimale : 1,
- valeur maximale : 200
- valeur par défaut : 50,
- couleur : rouge ("red").

![](img/26 Exo2.png){width="500"}

(*Le code est disponible sur le `bookdown`*)

<!--chapter:end:02-Un_exemple_pas_a_pas.Rmd-->

# **Interaction entre l’interface et le programme**

## Principe de fonctionnement

**Dans l’ui, on trouve :**

![](img/31 elements UI.png){width="750"}

---

**Dans le server :**

* On effectue des traitements à partir de paramètres d’entrée (`input$Annee`).
* À partir de ces résultats, les fonctions `render*()` alimentent les objets en sortie (`output$tableau`).
* Ceux-ci sont affichés dans l’interface (`ui`).

**NB:** `render*()` désigne toute fonction préfixée par render


## Circulation input - output

![](img/32 circulation iput output.png){width="750"}

## La réactivité

Qu'est-ce que c'est ?

La réactivité d'une application, c'est sa capacité à mettre à jour ses éléments suite à l'action d'un utilisateur.
<br>

**Schéma de la réactivité**

![](img/33 Reactivite.png){width="750"}

---

**La réactivité au plus court avec Shiny**

![](img/33 Reactivite au plus court.png){width="750"}


## L’interface utilisateur (UI)

L'`UI` (*User Interface*)  

- vitrine de l'application,  
- Saisie des informations : les `input`,  
- Définies par manipulation de `widgets`. 

- Affiche les résultats (`output`),  

Pour permettre à l'utilisateur de saisir des données en entrée (un `input`), on utilise des widgets. Les plus courants sont :

---

Widgets                       | Rôle
-- | -
textInput                     | zone de saisie pour une variable texte
numericInput                  | zone de "saisie" pour une variable numérique
selectInput                   | liste déroulante
radioButtons                  | bouton radio
checkboxInput                 | case à cocher
checkboxGroupInput            | ensemble de cases à cocher
sliderInput                   | bouton à déplacer sur une barre (curseur)
actionButton                  | bouton pour effectuer une action
fileInput                     | bouton pour sélectionner un fichier
shinyFiles::shinyFilesButton  |	bouton pour sélectionner des fichiers
shinyFiles::shinySaveButton   | bouton pour sauvegarder des fichiers

[Pour en savoir plus](https://shiny.rstudio.com/reference/shiny/)

---

![](img/34 Widgets.png){width="750"}

[Plus de widgets](https://shiny.rstudio.com/gallery/widget-gallery.html)

---

Les `widgets` sont des fonctions qui attendent plusieurs arguments. Les 2 premiers sont :

* `inputId` :	nom du widget
* `label`   :	titre du widget

**Exemple :** créer une liste déroulante (`selectInput`) nommée **select1** permettant d'effectuer un choix parmi les valeurs *a*, *b* et *c*.


```r
selectInput(
  inputId = "select1",
  label = "Veuillez faire un choix dans la liste",
  choices = c("choix 1" = "a", "choix 2" = "b", "choix 3" = "c")
)
```

![](img/37 select1.png){width="350"}

---

### **Exercice 3**

Ajouter, à l'application précédemment créée, un widget de type `textInput` qui permet de *modifier le titre du graphique*.
![](img/39 Exo3.png){width="750"}

(*Le code est disponible sur le `bookdown`*)  

On remarque que, à chaque saisie dans la zone de text, le graphique s'actualise avec un nouveau tirage aléatoire. On n'abordera ce sujet plus tard dans cette formation.



## Le serveur (`server`)

Le server contient le code `R` à exécuter pour effectuer tous les traitements nécessaires à l'application :

* Chargement de données
* Calculs
* Sélection, extraction
* Graphique, tableau...
* ...

## Les fonctions réactives

On ne peut écrire dans le server qu’à partir de fonctions réactives (`render*`, `reactive`, `eventreactive`, `observers`)

Dans le fichier `server`, faire appel à une valeur réactive nécessite une fonction réactive

![](img/36 Fonctions reactives.png){width="750"}

## Le package DT

Certains packages permettent de disposer d’autres fonctions `render`.  
Il y a par exemple le package `DT` qui améliore le rendu des tableaux.



```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)
```


```r
library(DT)
```

---


```r
# Chargement et traitement sur la table 
#des populations de 68 à 2015  dans l'environnement global de R
pop<- haven::read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
                rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee <- names(pop %>%  select_if(is.numeric))
```

---


```r
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une annee", choices=choix_annee)


  ,DTOutput("tableau")
)
```
<br>


```r
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderDT({
    pop %>% group_by(REGION) %>%
      summarise_at(input$annee,sum,na.rm=T)
  })
}
```

---


```r
# Run the application 
shinyApp(ui = ui, server = server)
```


![](img/38 DT.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

Des options peuvent être ajoutées avec la fonction `datatable()`. On crée ici la fonction *`formaterTableau`*.


```r
# Fonction pour formatter les tableaux
formaterTableau<-function(tab){
  datatable(tab, 
            rownames = F, 
            width="100%",
            extensions = c('Buttons','ColReorder','FixedColumns')
            ,options = list(
              dom = 'Blfrtip',
              buttons = c("colvis",'csv', 'excel', 'pdf'),
              colReorder = TRUE,
              lengthMenu = list(c(5, 25,50, -1), c('5', '25',"50", 'Tout')),
              pageLength = 10
            )
   )
}
```

---

On fait appel à `formaterTableau()` dans le `renderDT` correspondant.


```r
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderDT({
    pop %>% group_by(REGION) %>%
      summarise_at(input$annee,sum,na.rm=T) %>% 
    formaterTableau
  })
}
```

---

![](img/38 DT2.png){width="750"}

[Pour voir plus d'options (https://datatables.net/reference/option/)](https://datatables.net/reference/option/)
<br>

(*Le code est disponible sur le `bookdown`*)

## Le fonctionnement croisé

Les fonctions `render` côté **server** et `Output` côté **ui** vont de pair :

| ui                  | server
|---------------------|--------------
| `plotOutput`        | `renderPlot`
| `textOutput`        | `renderText`
| `tableOutput`       | `renderTable`
| `uiOutput`          | `renderUI`
| `verbatimTextOutput`| `renderPrint`
| `imageOutput`       | `renderimage`

---

### **Exercice 4**
**À partir de l'appli précédente, créer l'appli "Exo_4"**

* Ajouter un widget de type `selectInput` qui permet de sélectionner la couleur du graphique,
* Ajouter un widget de type `selectInput` qui permet de sélectionner la couleur des bordures des barres de l'histogramme.

---

![](img/40 Exo4.png){width="750"}

---

### **Exercice 5**

* Modifier les widgets précédemment créés pour afficher les couleurs en Français. 

![](img/40 Exo5.png){width="650"}

(*Le code est disponible sur le `bookdown`*)

<!--chapter:end:03-Interaction_UI_Server.Rmd-->

# **Gestion de la mise en page d’une application avec** `shinydashboard`

Le package `shinydashboard` permet de construire des applications avec une interface graphique de type tableau de bord.

---


```r
# app.R 
library(shiny)
library(shinydashboard)
ui <- dashboardPage(
  dashboardHeader(),
  dashboardSidebar(),
  dashboardBody()
)
server <- function(input, output) { }
shinyApp(ui, server)
```

![](img/41 dashboard vide.png){width="650"}

On peut ensuite personnaliser chacune de ces 3 parties. 
La fonction `dashboardHeader()` permet de personnaliser l’en-tête de l’application.

## dashboardheader()

### Ajouter un titre

Un en-tête de tableau de bord peut rester vide ou inclure :

* le titre de l’application,
* des éléments de menu déroulant à droite.

---

Ici, on ajoute le titre de l'application avec le paramètre `**title() : « formation shiny »**`


```r
# app.R 
library(shiny)
library(shinydashboard)
ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  dashboardSidebar(),
  dashboardBody()
)
server <- function(input, output) { }
shinyApp(ui, server)
```


![](img/41 dashboard titre.png){width="750"}

---

### Ajouter un menu

Les menus déroulants sont générés par la fonction `dropdownMenu()`. Chacun des `dropdownMenu()` doit être renseigné avec un type d'élément correspondant :

* `messages`,
* `notifications`
* `tasks`

![](img/42 dashboard dropdownmenu.png){width="750"}

---

Ci-dessous, on ajoute un menu déroulant de type "messages" qui comprend 3 messages.
La fonction `messageItem()` a besoin, au minimum, des paramètres :

- `from` 
- `message`  
<br>

Vous pouvez également contrôler l'**icône** et une chaîne de **temps de notification** (heure, date). 


```r
dashboardHeader(title = "formation shiny",
```



```r
dropdownMenu(type = "messages",
             messageItem(from = "message 1", message = "texte message 1"),
             messageItem(from = "message 2", message = "texte message 2",
                         icon = icon("question"),  time = "13:45"),
             messageItem(from = "message 3", message = "texte message 3",
                         icon = icon("life-ring"), time = "2014-12-01")
),
```

---

![](img/42 dashboard messages.png){width="550"}

---

Ici, on ajoute un menu déroulant de type `notifications` qui comprend 3 notifications avec les paramètres :

- `text` 
- `icon`



```r
dropdownMenu(type = "notifications", badgeStatus = "warning",
             notificationItem(text = "2627 utilisateurs",
                              icon("users")),
             notificationItem(text = "187 articles",
                              icon("truck"),
                              status = "success"),
             notificationItem(text = "Problemes de connexion ce week-end",
                              icon = icon("exclamation-triangle"),
                              status = "warning")
)
```

---

![](img/42 dashboard notifications.png){width="550"}

---

Ici, on ajoute un menu déroulant de type `tasks` qui comprend 4 tâches : chacune a un taux de réalisation différent. On ajoute les paramètres :

- `value` 
- `color`



```r
dropdownMenu(type = "tasks",
             badgeStatus = "success",
             taskItem(value = 5, color = "purple", "guide pédagogique"),
             taskItem(value = 90, color = "green", "exercice de formation"),
             taskItem(value = 60, color = "red", "exercice de synthese"),
             taskItem(value = 80, color = "yellow", "support de cours")
)
```

---

![](img/42 dashboard tasks.png){width="550"}

(*Le code est disponible sur le `bookdown`*)

---

Dans la plupart des cas, vous souhaiterez dynamiser le contenu de ces « boites à messages ». Cela signifie que le contenu HTML est généré côté serveur et envoyé au client pour le rendu.

* 1 - En amont, vous chargerez le fichier des messages (ici, `message.ods`). C'est un fichier avec différentes colonnes : au minimum de quoi compléter les paramètres `from` et `messages`. 


```r
messageData <- rio::import("donnees/messages.ods")
```


  ![](img/42 dashboard messages ods.png){width="450"}

---

* 2 - Dans le code de l'interface utilisateur, vous utiliserez `dropdownMenuOutput()`.


```r
dashboardHeader(
  title = "formation shiny",
  dropdownMenuOutput("messageMenu")
)
```

---

* 3 - Et côté serveur, vous générerez l’ensemble du menu dans une fonction de type renderMenu()


```r
server <- function(input, output) { 
  output$messageMenu <- renderMenu({
    msgs <- apply(messageData, 1, function(row) {
      messageItem(from = row[["from"]], message = row[["message"]],time =row[["time"]] )
    })
    dropdownMenu(type = "messages", .list = msgs)
  })
}
```

---

![](img/42 dashboard messages 2.png){width="750"}

Pour plus de renseignements sur les autres types de menus déroulants (`notifications` et `tâches`), cf le [site shinydashboard / structure (https://rstudio.github.io/shinydashboard/structure.html))](https://rstudio.github.io/shinydashboard/structure.html)

(*Le code est disponible sur le `bookdown`*)

Sur les forums, on peut également trouver des exemples pour alimenter ces différents menus directement à partir de l'appli.

---

#### **Exercice 6-1**
Réaliser cette application

![](img/43 Exo 6_1.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

## `dashboardSidebar()` : créer des menus

La fonction `dashboardSidebar()` permet de personnaliser la barre latérale en ajoutant :

* des menus,
* des boutons ...
	
Nous pouvons donc ajouter du contenu à la barre latérale. Par exemple :

* des éléments de menu qui se comportent comme des onglets.

---

- Page 1 : en cliquant sur l’onglet **Accueil**, on affiche la page correspondante.

![](img/44 dashboardsidebar.png){width="750"}	

---

- page 2 : On affiche la page **Table**.  

  ![](img/44 dashboardsidebar_Table.png){width="750"}

---

On peut aussi ajouter des widgets, boutons, des messages, images ...

Ces éléments sont visibles quelle que soit la page sur laquelle vous travaillez.  

**Par exemple :** un bouton «Stop». 

(*Le code est disponible sur le `bookdown`*)

---

Dans le `dashboardSidebar`, on initie les différents onglets avec la fonction `menuItem`


```r
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil"),
      menuItem(tabName="onglet2", text ="Table")
    )),
```

---

Dans le «corps» de l’appli (`dashboardBody`), il faudra alors personnaliser chacun des `menuItem`.  

On ajoutera tout d’abord la fonction `tabItems()` dans lequel on définira :  

- 1 `tabItem` par `menuItem` initié ci-dessus.  
- La relation entre les 2 se fait par leurs identifiants : les `tabName`  


```r
  dashboardBody(
    tabItems(
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue")),
      tabItem(tabName="onglet2",
              h1("Bienvenue sur la page de données tabulées", style="color:blue"))
      )
  )
```

---

On peut encore personnaliser : badges, couleur de badge...


```r
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil",
               badgeLabel = "Un", badgeColor = "red",icon = icon("bath")), br(),
      
      menuItem(tabName="onglet2", text ="Table",
               badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    )
  )
```

![](img/44 dashboardsidebar2.png){width="550"}	

(*Le code est disponible sur le `bookdown`*)

---

### **Exercice 6-2**
Réaliser cette application. Consignes :

![](img/44 Exo 6_2.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

## `dashboardBody()` : afficher des données

Pour personnaliser le corps d’une appli, on peut ajouter différents types d’éléments:

* `box()`,
* `tabBox()`,
* `infoBox()`

---

### les `box`


```r
  dashboardBody(
    tabItems(
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue"),
              
              box(id="box1", status = "primary", solidHeader = TRUE ,
                  background = "light-blue", title = "chargement des données",
                  collapsible = TRUE,collapsed = F,
                  
                  h4("Box() de sélection des données")
              ),
              box(id="box2", status = "success", solidHeader = TRUE ,
                  title = "tables de données", width = 12, collapsible = TRUE,
                  collapsed = FALSE,
                  
                  h4("Box() d'affichage des données")
              )
              )))
```

---

![](img/45 box.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

**Les principaux paramètres des** `box()`

|Paramètre        | Paramètres          | Explications                                             |
|-----------------|---------------------|----------------------------------------------------------|
|`status`         | primary (bleu), success(vert), info(bleu), warning (orange), danger (rouge) |permet de définir une couleur de box() et représenter un état/statut|
|`solidHeader`    | TRUE, FALSE         | Faut-il afficher la barre d’en-tête, avec la couleur ?   |
|`background`     | red, yellow, aqua, blue, light-blue, green, navy ... | couleur de l’arrière plan  |
|`title`          | titre (optionnel)   | titre à afficher dans l’en-tête                          |
|`Width`          |                     | la taille sur une grille de 12 unités                    |
|`collapsible`    | TRUE, FALSE         | Si TRUE, affiche un bouton en haut à droite permettant à l’utilisateur de réduire la zone |
|`collapsed`      | TRUE, FALSE         |  Si TRUE, la `box()` est déployée                        |


---

Les sorties peuvent également être intégrées dans des `box()`

![](img/45 box sorties.png){width="750"}

---

**dans l'**`UI`


```r
box(id="box2", status = "success", solidHeader = TRUE , 
    title = "Table de données", width = 12, collapsible = TRUE, collapsed = FALSE,
    DTOutput("tableau")
    )
```

**Dans le** `server`**, pas de changement**


```r
output$tableau <- renderDT({ 
  pop %>%  group_by(REGION)  %>% summarise_at (input$ANNEE, sum,na.rm=TRUE) 
})
```

---

**Dans l'environnement global**


```r
library(DT)
library(shiny)
library(shinydashboard)	

pop <- readRDS("donnees/poplegale_6815.RDS")

formaterTableau<-function(tab){
  datatable(tab, 
            rownames = F, width="100%",
            extensions = c('Buttons','ColReorder','FixedColumns')
            ,options = list(
              dom = 'Blfrtip',
              buttons = c("colvis",'csv', 'excel', 'pdf'),
              colReorder = TRUE,
              lengthMenu = list(c(5, 25,50, -1), c('5', '25',"50", 'Tout')),
              pageLength = 10
            )
   )
}
```

(*Le code est disponible sur le `bookdown`*)

---

#### **Exercice 6-3**

**Réaliser l'interface suivante**

![](img/45 Exo 6_3.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

### les `tabBox`

Pour créer une tabBox, 2 mots clés :

* `tabBox` et `tabPanel` pour définir les différents panneaux/onglets de la boîte.

Ici, on va créer, dans un panneau **Table**, une boite avec 5 onglets :

* « Statistiques »,
* «  table de données »,
* « Graphiques »,
* « Cartes »,
* « Info diverses ... »

---

![](img/46 tabbox.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

A l’instar de ce qu’on fait pour les `box()`, on va ajouter différents widgets et sorties à afficher.

- Onglets **"Statistiques"**, **"table de données"** et **"Info diverses"** : un tableau; 
- Onglets **"Graphiques"** et **"cartes"** accueilleront des objets de types `plot`.


```r
tabBox(
  title = "Donnees filtrees", width = 12, id = "tabBox1",
  
  tabPanel(title = "Statistiques", DTOutput("filtreStats")),
  
  tabPanel(title = "table de donnees", 
           h4("Table des donnees filtrees", style="color: SteelBlue"),
           hr(),
           DTOutput("filtreTable") ),
  
  tabPanel(title = "Graphiques", plotOutput("graph")),
  
  tabPanel(title = "Cartes", plotOutput("Carto")),
  
  tabPanel(title = "Informations diverses (Variables & Concepts)",
           DTOutput("infoDiv"))
)
```

---

#### **Exercice 6-4**

![](img/46 Exo 6_4.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

### les `infoBox()`

Elles permettent d’afficher des valeurs numériques ou textuelles simples, avec une icône. Dans l'exemple suivant, on affiche 3 `infoBox()` permettant de visualiser :  

* la date du jour,
* l'IDEP de l'utilisateur,
* une boîte permettant de lancer la page web "shinydashboard"

---

Ces boîtes sont visibles à partir de n’importe quelle page de l’application.

![](img/47 infobox.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

l’`infoBox()` peut être dynamique ou non. On la définit dans l'`UI` si elle est statique ou dans le `server` au sein d'un `renderInfoBox()` si elle est dynamique.

**Non dynamique :**

* `infoBox()`,
* les valeurs à afficher sont mises dans le paramètre `value =`


```r
dashboardBody(
  fluidRow(
    infoBox(title = "Date", value = Sys.Date(), icon = icon("credit-card")),
    infoBox(title = "IdepUser", value = toupper(Sys.getenv("USERNAME")), icon = icon("credit-card")),
    infoBoxOutput(outputId = "siteInternet", width = 4)
  ),
```

---

**Dynamique (Exemple : siteInternet):**
On utilise :

* `infoBoxOutput()` dans la partie `UI` où on définit un `outputId`,
* on définit la sortie de type `renderInfoBox()` dans le `server` où on intègre l'`infoBox()`.


```r
 output$siteInternet <- renderInfoBox({
    infoBox(
      " ",value="shinydashboard : le site", icon = icon("thumbs-up"),
      color = "blue", fill = TRUE, href = "https://rstudio.github.io/shinydashboard/index.html"
    )
```

---

#### **Exercice 6-5**

**Reproduire cette appli.**

![](img/47 Exo 6_5.png){width="750"}

(*Le code est disponible sur le `bookdown`*)


## Divers éléments de mise en page

### column(), fluidRow(),

- L'implémentation `Shiny` du `Responsive design`  :  

  - **FluidRow()**    
    - Création d mises en page fluides. 
      - des lignes qui incluent à leur tour des colonnes. 
      - Les pages fluides redimensionnent leurs composants en temps réel pour couvrir toute la largeur disponible du navigateur.

  - **Column()**  
    - `Width :` la largeur de la colonne (sur un total de 12 colonnes).  
    - `Offset :` décaler la position des colonnes pour obtenir un contrôle plus précis 
    
      - Déplacer les colonnes vers la droite
      - Chaque unité de décalage augmente la marge gauche d'une colonne.

---

* **Un 1er exemple simple**

    - Exécuter le code suivant dans `RStudio`;
    - Ajuster la largeur de la fenêtre pour voir comment la page s’adapte;
    - Noter qu’en plus d’empiler les colonnes d’une même ligne, le texte est également ajusté pour éviter l’ajout d’une barre de défilement horizontale.


```r
library(shiny)
ui <- fluidPage(
  title = "Hello Shiny!",
  fluidRow(
    column(width = 4,
           "Colonne 1 --> colum(width = 4)"
    ),
    column(width = 4, 
           "Colonne 2 --> colum(width = 4) ... Un peu de texte pour passer sur
           2 lignes"
    ),
    column(width = 4, 
           "Colonne 3 --> colum(width = 4, offset=3) ... mais on rajoute un peu
           plus de texte pour illustrer le retour automatique à la ligne.
           Profitez-en pour redimensionner votre fenêtre.
           C'est magique, c'est responsive design !"
    )
    )
  )

shinyApp(ui, server = function(input, output) { })
```

---

* **Un 2ème exemple plus élaboré**

![](img/48 column 1.png){width="750"}


```r
tabPanel(title="divers",
         h2("colonne 1", style="color:blue"),
         h3("colonne 2", style="color:green"),
         h4("Colonne 3",style="color:red")
)
```

Par défaut, « les colonnes » se positionnent les unes en dessous des autres (car elles ont une largeur de 12).

(*Le code est disponible sur le `bookdown`*)

---

La fonction `column` permet de fixer la taille des éléments sur une grille de 12. En attribuant une taille de 4 à chacun des trois éléments, ces derniers sont disposés côte à côte

![](img/48 column 2.png){width="550"}


```r
tabPanel(title="divers",
         column(width = 4,h2("colonne 1", style="color:blue")),
         column(width =4,h3("colonne 2", style="color:green")),
         column(width =4,h4("Colonne 3",style="color:red")))
```


(*Le code est disponible sur le `bookdown`*)

---

On peut imbriquer les column() pour façonner l’interface selon la volonté du développeur et/ou du concepteur

![](img/48 column 3.png){width="550"}


```r
tabPanel(title="divers",
             column(width=6, column(width=12,offset = 3, h3("colonne 1", style="color:black")),
                         column(6,h3("colonne 1A", style="color:blue")),
                         column(6,h3("colonne 1A", style="color:red"))
              ),
              column(3,h3("colonne 2", style="color:green")),
              column(3,h3("Colonne 3",style="color:red"))
               )      
)
```

(*Le code est disponible sur le `bookdown`*)

---

### Eléments statiques

| Actions                         | Fonctions                         |
|-------------------------------- | --------------------------------- |
| Incorporer un lien web          | a()                               |
| Passer une ligne                | br()                              |
| Créer un paragraphe             | p()                               |
| Mettre en italique              | em()                              |
| Mettre en gras                  | strong()                          |
| Utiliser un style (de 1 à 6)    | h1(), h2(), h3(), h4(), h5(), h6()| 
| Ajouter une ligne horizontale	  | hr()                              |
| Ajouter une image               | img()                             |

---

![](img/49 elements statiques.png){width="550"}


```r
p(style = "font-family:Impact", "Accéder aux pages Web de", br(),
  a("@ R Shiny", href = "https://shiny.rstudio.com/"), br(),
  a("@ R Shiny dashboard", href = "https://rstudio.github.io/shinydashboard/"),
  hr()
)
```

(*Le code est disponible sur le `bookdown`*)


---

#### **Exercice 6-6**

**Réaliser l'interface suivante.**

![](img/49 Exo 6_6.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

<!--chapter:end:04-Mise_en_page.Rmd-->

# **Les fonctions du server**

## La gestion des `input`

- S'assurer que les valeurs des input sont bien disponibles

  - Utilité de la fonction `req()`

Si on ne mets pas de conditions, on aura des messages d'erreur :


```r
fileInput("chemin_tab","Selectionner le fichier RDS",
                           buttonLabel = "Charger..." )
```


```r
 output$tableau <- renderDT({
    readRDS(file =input$chemin_tab$datapath )
  })
```

---

![](img/51 req 1.png){width="750"}
<br>

Il y a un message d’erreur, si le fichier n’est pas chargé

(*Le code est disponible sur le `bookdown`*)

---

En utilisant le req(), il n’ y a plus de message d’erreur, si le fichier n’est pas chargé


```r
 output$tableau <- renderDT({
readRDS(file =req(input$chemin_tab$datapath) )
  })
```

![](img/51 req 2.png){width="750"}

---

Une fois le fichier chargé

![](img/51 req 3.png){width="750"}

(*Le code est disponible sur le `bookdown`*)


## Opérer traitements, créer objets à partir valeur réactive en amont des fonctions réactives `render*()`

---

### Un exemple pour y croire

On souhaite faire : 

* un tableau
* un graphique 

    - à partir d’un même tirage

---

Dans ce cas, les 2 `Outputs` sont construits à partir de 2 tirages différents

**Dans le** `server`

```r
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = "skyblue", color = "black",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
```


```r
  output$resumeStat <- renderPrint({
    summary(rnorm(input$nbvaleurs))
  })
```

Dans chacune des fonctions `render` réalisant **l'histogramme** et le **récapitulatif des valeurs**, un nouveau tirage aléatoire est réalisé.  

La **modification d'une valeur réactive** engendre le déclenchement de **toutes les fonctions réactives** dans lesquelles elle est impliquée.

---

**1er tirage**
![](img/52 exemple pour croire 1.png){width="750"}

---

**2ème tirage**
![](img/52 exemple pour croire 2.png){width="750"}
<br>

=> **On a deux tirages différents**

(*Le code est disponible sur le `bookdown`*)

---

**On fait un tirage différent pour chaque Output**


```r
  output$resumeStat1 <- renderPrint(
    #nombre()
    rnorm(input$nbvaleurs)
  )
```

![](img/52 exemple pour croire 3.png){width="750"}


```r
  output$resumeStat2 <- renderPrint(
    #nombre()
    rnorm(input$nbvaleurs)
  )
```

**Les deux distributions ne sont pas identiques** 

((*Le code est disponible sur le `bookdown`*))
<br>

<span style="background-color:lightBlue">**Comment construire les deux `Output` à partir des mêmes données ?** </span>

---

### Les fonctions réactives

* en utilisant une fonction `reactive()` pour construire et retourner un résultat : ici celui de la fonction `rnorm()`. 


```r
nombre <- reactive(rnorm(input$nbvaleurs) )
```
`nombre()` est l'expression réactive intermédiaire.  


```r
output$resumeStat1 <- renderPrint(
  nombre()
)
```


```r
output$resumeStat2 <- renderPrint(
  nombre()
)
```

---

On appelle cette fonction `nombre()` dans chaque `output` concerné.
A chaque modification de l’`input$nbvaleurs`, le tableau statistique et l’histogramme seront générés à partir des mêmes résultats et les deux distributions seront désormais identiques.

![](img/52 exemple pour croire 5.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

![](img/52 exemple pour croire 6.png){width="750"}

- On utilise des fonctions `reactive()` pour travailler en amont des fonctions de type `render*()`.
- elles permettent d’utiliser plusieurs fois un même résultat, pour lequel il y a qu’un seul traitement.
- Ce sont des intermédiaires entre `input$` et `output$`.

---

**Dans notre exemple, pourquoi cela règle le problème ?**
La fonction `reactive()` (`nombre()`) va permettre de ne lancer qu'une seule fois le tirage des valeurs aléatoires.
Les objets qu'elle retournera seront utilisées dans chacune des deux fonctions `render*()` : celles-ci se lanceront donc sur la même distribution.

![](img/53 Schema.png){width="750"}

Une fonction  `reactive()` garde en mémoire la dernière valeur calculée à partir de ses `inputs`. 
On peut appeler plusieurs fois le résultat de la fonction, elle ne refait pas de calculs.

---

#### **Exercice 7**
A partir du code récupéré (bookdown), adapter le programme pour créer un bouton qui permette de charger les données (`poplegale_6815.RDS`), comme dans l'exemple ci-dessous.

(*Le code est disponible sur le `bookdown`*)

**Consignes :**  

- créer un `fileInput`  
- créer une fonction reactive qui sera utilisée pour générer les outputs `tableau` et `tableau2`  

![](img/53 Exo 7.png){width="750"}

((*Le code est disponible sur le `bookdown`*))

---

#### **Exercice 8**
A partir du code récupéré (bookdown), adapter le programme pour que le graphique ne change pas chaque fois qu’on change la couleur.


**Consignes :**
- créer un `fileInput`  
- créer une fonction reactive qui sera utilisée pour générer les outputs `tableau` et `tableau2`  

(*Le code est disponible sur le `bookdown`*)


![](img/53 Exo 8.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

## Attendre la validation de l'utilisateur avant la mise à jour des sorties

### Les fonctions `eventreactive()`

**Contexte :**
On souhaite que la mise à jour des sorties d'une application soit mise en attente jusqu'à la validation de l'utilisateur, par clic sur un bouton

**Avant validation**

![](img/54 eventReactive 1.png){width="750"}

---

**Après validation**

![](img/54 eventReactive 2.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

**Fonctionnement**

* 1- On crée le bouton de validation avec la fonction actionButton()


```r
actionButton("validbouton","Validez votre choix")
```

* 2- On crée une expression réactive dont la réactivité est conditionnée à la validation du bouton d'action, avec la fonction eventReactive()


```r
nombre <- eventReactive(input$validbouton , rnorm(input$nbvaleurs) )
```

* 3- le résultat du`eventReactive` est utilisé dans la fonction `renderPlot`


```r
  output$graphique <- renderPlot(
    gf_histogram(~ nombre(),fill = "skyblue", color = "black",
                 title="Graphique") %>% 
    gf_theme(plot.title = element_text(hjust = 0.5))
  )
```

---

La fonction `eventReactive()` permet de créer une expression réactive qui ne s'exécute que lorsque le bouton a été validé

![](img/54 eventReactive 3.png){width="750"}

---

![](img/54 eventReactive 4.png){width="750"}

---

#### **Exercice 9**

A partir du code récupéré (bookdown), adapter le programme pour que l’utilisateur puisse choisir d’afficher deux types de graphiques différents en important la table **Projection.xls**. Prévoir un bouton de validation pour que le graphique se mette à jour.

<details>
<summary markdown="span">**Consignes supplémentaires (si besoin)**</summary>

- créer des `radioButtons` proposant 2 types de graphique ("g1" ou "g2") à afficher,  
- définir la construction du graphique dans de 2 fonctions `reactive` nommée `g1` et `g2`  
- créer un `eventreactive` permettant de créer l'output selon le bouton coché  


```r
    r_tab() %>% mutate(ANNEE=as.numeric(ANNEE),POP=as.numeric(POP)) %>%  
      filter(DEP==req(input$D)) %>% 
      gf_point(POP ~ ANNEE) 
```
</details>

---

![](img/54 eventReactive Exo9.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

### **Focus** *`shinyFileButton()`* / *`shinyFileChoose()`*

![](img/55 shinyfilechoose.png){width="750"}

## Effectuer des actions

### Les fonctions observers

On va expliciter le rôle de différents types d'`observers` : les fonctions `observe()` et `observeEvent`.

- **Utilité des `observers` dans des applications réactives :**

  * Pour le gestionnaire de l'application (log, sauvegarde...)
  * Réaliser des traitements sans que ceux-ci n'ait de conséquences sur les éléments utilisés et affichés pour l'utilisateur
  * Modifier l’interface

Les `observers` permettent de déclencher une action lorsqu’un champ est modifié.

**Il existe 2 cas :**

---

* `observe()`: est appelé à chaque modification de la valeur d’un input cité dans la fonction.

![](img/56 observe.png){width="750"}
<br>

*Le code est disponible sur le `bookdown`* : tester ce code et observer le résultat dans votre console

---

* `observeEvent()`: est appelé à chaque modification de la valeur d’un `input` cité dans le 1er argument de la fonction.

![](img/56 observeEvent.png){width="750"}
<br>

(*Le code est disponible sur le `bookdown`*)

---

* **observe()**


```r
observe(print(input$nbValeurs))
```
le `print()` s’exécute à chaque modification de la valeur de `input$nbValeurs`. Il est donc exécuté à la modification d'un `input`.

* **observeEvent()**


```r
observeEvent(input$validBouton, print(input$nbValeurs))
```
le `print()` s’exécute à chaque « clic » sur le bouton `input$validBouton`.

Il est donc exécuté à la validation d'une donnée par un clic sur l'`input` de validation.

---

### Exemples d’utilisation des observers

**Fermer l'application avec un bouton*


```r
library(shiny)
library(ggformula)
library(dplyr)

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
              label="Choisir un nombre de valeurs",
              value=50, min=1,max=500),
  actionButton("stop","Stop")
  
)
#serveur de calculs
server <- function(input, output) {
  observe( print(input$nbvaleurs) )
  observeEvent(input$stop,stopApp())
}

# Run the application 
shinyApp(ui = ui, server = server)
```

La fonction `observeEvent` déclenche l'action lors d'un clic sur le bouton.

---

**Exemple d'utilisation de l'`observeEvent`**

![](img/56 observeEvent graph.png){width="750"}

Le bouton *Validez votre choix* permet de générer le graphique : l’`output` *graphique* est créé quand on clique sur le bouton.  

(*Le code est disponible sur le `bookdown`*)

---

**sauvegarder une table, sans choisir le chemin** : `ggsave`

![](img/56 observeEvent ggsave.png){width="750"}

- On récupère un dataframe qui contient le chemin sélectionné par l’utilisateur. On l’utilise ici pour 
paramétrer la fonction `ggsave`.  

- La table est sauvegardée au format `.RDS` dans le dossier `donnees` de ce projet.

(*Le code est disponible sur le `bookdown`*)

---

#### **Exercice 10**

A partir du code ci-dessous, modifier le programme pour :  

- Créer un bouton pour le titre du graphique  
- Sauvegarder, à partir d’un bouton, la table importée :  

  - Nom donné au graphique + `_save.RDS`.  

- table à télécharger : `projection.xls`

![](img/56 observeEvent Exo 10.png){width="650"}

(*Le code est disponible sur le `bookdown`*)

## Transformer une valeur réactive en valeur non réactive

### Un exemple pour y croire


```r
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = couleur, color = "black",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
  
}
```


La fonction `renderPlot()` fait appel à deux valeurs réactives :

* input$nbValeurs
* input$titreGraphique

Une fonction dynamique se relance entièrement chaque fois qu'une des valeurs réactives qu'elle utilise est modifiée

---

![](img/57 isolate 1.png){width="750"}

(*Le code est disponible sur le `bookdown`*)
<br>

<span style="background-color:lightBlue">**Comment faire en sorte que la mise à jour du titre du graphique ne vienne pas relancer l'ensemble de la fonction réactive ?** </span>

---

### La fonction isolate

En utilisant la fonction `isolate()`. Elle permet de transformer une valeur réactive en valeur non réactive.

**Syntaxe :**

```r
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = couleur, color = "black",
                 title=isolate(input$titreGraphique)) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
```

Ainsi, dans l’exemple, en modifiant le titre, celui-ci ne se mettra à jour qu'à l'occasion d'un rafraîchissement de la page Web ou bien lors de la prochaine modification de l'autre valeur réactive (qui relancera la fonction `renderPlot()`)

---

![](img/57 isolate 2.png){width="750"}

(*Le code est disponible sur le `bookdown`*)

---

**Ce qu'un concepteur d'application pourrait vouloir faire :**
<br>

* utiliser une valeur réactive à plusieurs reprises dans la partie `server` 
* faire des traitements avec la valeur réactive avant de l'envoyer dans une fonction `render*` 
    - => `reactive()`
* attendre la validation de l'utilisateur avant la mise à jour des sorties 
    - => `observEvent()` et `eventReactive()`
* réaliser des traitements qui ont lieu dans la partie `server` sans conséquence sur l'`ui`
    - => `observe()`
* transformer une valeur réactive en valeur non réactive
    - =>`isolate()`

<!--chapter:end:05-Les_fonctions_du_server.Rmd-->

# *Interfaces  dynamiques*

Plusieurs approches d'interface dynamique :

* renderUI+uiOutput, qui permettent de générer dynamiquement des appels à des `Input`
* afficher/masquer des widgets (`conditionalPanel()`)

## `renderUI` et `uiOutput`

- Générer dynamiquement l'interface et le contenu des widgets,  

- `renderUI()` permet de modifier l’interface depuis le `server`.  

  - Dans l’`UI`, on utilise la fonction `uiOutput()`.

---

**Exemple 1 :**

<span style="background-color:lightBlue">
l'utilisateur veut sélectionner un département. Il choisit d'abord la région concernée,
puis l'interface est mise à jour automatiquement pour lui proposer seulement les départements de cette région.
</span>

(*Le code est disponible sur le `bookdown`*)

---

<span style="background-color:lightBlue">
**Exemple 2 et 2 bis (avec output `tableau`) :**
</span>

* créer une liste déroulante avec la liste des départements ou des régions du fichier chargé,
* afficher une 2ème liste déroulante qui ne s’affiche que lorsque les départements ou les régions sont sélectionnés avec les variables numériques du fichier.

(*Le code est disponible sur le `bookdown`*)

---

<span style="background-color:lightBlue">
**Exemple 3 : avec un `tagList`**
</span>

**Ui :**

```r
radioButtons("export.type", "",
             c("Exporter la table courante" = 1,
               "Exporter une liste de tables (Excel xlsx)" = 2,
              "Sauvegarder une liste de tables (Rdata)" = 3))
```


```r
uiOutput("export.control")
```

**Server :**

```r
output$export.control <- renderUI(
  if (input$export.type>1)
    tagList(
      selectizeInput("export.tables",
                     if (input$export.type == 2) "Tables à exporter" 
                     else "Tables à sauvegarder", multiple = TRUE,
                     options = list(placeholder = '<toutes>'), choices= listeTables),
      if (input$export.type==2) checkboxInput("export.names",
                                              "Préciser le nom des feuilles", FALSE) 
    ))
```

(*Le code est disponible sur le `bookdown`*)

---

### Exercice 11

Copier et adapter le code ci-dessous.
Dans l'**onglet 2**, créer les widget suivants :

* Choix de la ou les région(s),
* Choix des départements correspondant aux régions sélectionnées.
* Choix de la population à afficher
    - puis afficher la population par département qui correspond à la sélection

Il vous faudra préalablement charger, dans l'onglet 1, la table `poplegale_6815.RDS`.

(*Le code est disponible sur le `bookdown`*)

---

![](img/57 isolate Exo 11.png){width="750"}

(*Le code est disponible sur le `bookdown`*)


## `conditionalPanel`

Le `conditionalPanel` permet d'afficher un élément de l'interface (`widget` ou `output`) en fonction d'une (ou plusieurs) condition(s).

**Exemple (partiel) :**

```r
radioButtons(inputId = "graph",label = "voulez vous paramétrer le graphique ?",
             choices = c("oui","non"),selected = "non")
```

La condition est entre **" "** et contient un **.** au lieu d'un **$**.

```r
conditionalPanel(
  condition = "input.graph=='oui'",
  (sliderInput("bins",
                "Number of bins:",
                min = 1,
                max = 50,
                value = 30)
  ))
```

---

![](img/58 ConditionalPanel.png){width="750"}

(*Le code est disponible sur le `bookdown`*)


<!--chapter:end:06-shinyfiles_et_interface_dynamique.Rmd-->

# Bon à savoir

## Scinder le code source (package `Golem`)

- Modulariser son code   

  -  développer en modules. Pour cela, 
  - faciliter le développement de votre application :
    - ajouter un module,
    - ajouter votre template CSS 
    - ajouter du Javascript…

- Il peut être utile de savoir comment créer un package. 

- Conseils :  modules shiny modules
  - [Pour plus d'infos, on peut voir ici](https://www.ardata.fr/post/2019/02/11/why-using-modules/)


---

### Principes de base

---

#### Installation et Exécution de `Golem`


```r
install.packages("golem")
```

#### Créer son package d'application `shiny`  

Une fois le package installé, vous pouvez accéder à *Fichier*> *Nouveau projet…* dans RStudio, et choisir l'entrée *Package pour Shiny App utilisant golem*»*.

![](img/60 golem.png){width="650"}

---

#### Préparation

- **`dev/01_start.R`**  

Une fois que vous avez créé votre projet, le premier fichier qui s'ouvre est `dev/01_start.R`. Ce fichier contient une série de commandes que vous devrez exécuter une fois, au début du projet.  

Notez que vous n'êtes pas obligé de tout remplir, l'bien que fortement recommandé.  

    - Remplissez la `DESCRIPTION`  

Tout d'abord, remplissez la `DESCRIPTION` en ajoutant des informations sur le package qui contiendra votre application.  

    - Ajouter les `options`, les packages utilisés, divers outils  
    - Pour exécuter l'application, exécuter le fichier `dev/run_dev.R`.  

---

#### Développement

- **`dev/02_dev.R`**  

    - Ajouter des dépendances  
    - Ajouter des modules qui contiendront, chacun, une partie de l'UI et du server  
    - Ajouter des fonctions de logique "métier"  
    - Ajouter des fichiers externes : img, CSS, JS...  
    - Ajouter des données  

---

#### Déploiement

- **`dev/03_deploy.R`**  
contient la fonction de déploiement sur diverses plates-formes  

    - RStudio
    - Docker


## Créer un exécutable pour lancer l'application

1- Créer un programme R (ici, runApp ExerciceFinal.R) avec :

* au minimum, la librairie `shiny`
* le `runApp()` en renseignant le chemin du script à exécuter (ici, FormationDashboard ExerciceFinal.R).
 
![](img/59 runApp 2.png){width="650"}
 
---
 
2- Créer un fichier "text" (avec notePad++ par exemple) :

* Ajouter les chemins pointant vers :
    - la version de R utilisée (ici, R-3.3.3),
    - le script contenant la fonction runApp() (ici, run App ExerciceFinal.R)
        - pause signifie que la fenêtre restera ouverte, ce qui est utile pour debugger votre appli si quelque chose se passe mal,
        - exit (à la place de pause) pour que la fenêtre se ferme automatiquement avec l'appli.
* l'enregistrer et le nommer (ici, Executer ExerciceFinal)
* modifier son extension en : .bat

---

![](img/59 runApp 3.png){width="750"}

## Créer un raccourci pour lancer l’application

1- Créer un programme R avec :

* les librairies
* le `runApp` en renseignant le chemin de l’application

![](img/59 runApp.png){width="650"}

<!--chapter:end:07-Bon_a_savoir.Rmd-->

