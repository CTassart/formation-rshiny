# Formation RShiny  
**Work In Progress**  

## Supports de la formation "Concevoir une application web dynamique avec R et le package shinydashoard" - Insee

### Accessibles **hors Insee** :  

|format|url|Etat|
|---|---|---|
|html | https://ctassart.gitlab.io/formation-rshiny/shiny_SupportFormation_html | STOP |
|Bookdown de la formation|https://ctassart.gitlab.io/formation-rshiny-bookdown/|A relire|
|Diaporama de la formation | https://ctassart.gitlab.io/formation-rshiny/formation-shiny-bookdown | WIP |
|Notice création service RStudio du SSP Cloud|https://ctassart.gitlab.io/formation-rshiny/SSPCloud_RStudio.html|WIP|

### Accessibles réseau **Insee** :  


|format|url|Etat|
|---|---|---|
|Diaporama de la formation | http://t19qhl.gitlab-pages.insee.fr/formation-rshiny/formation-shiny-bookdown | WIP |
|Notice création service RStudio du SSP Cloud|http://t19qhl.gitlab-pages.insee.fr/formation-rshiny/SSPCloud_RStudio.html |WIP|

