---
title: "Formation RShiny html"
author: "Cédric Tassart"
date: "12/06/2020"
output: 
  ioslides_presentation: 
    fig_caption: yes
    highlight: zenburn
    keep_md: yes
    smaller: yes
    widescreen: no
  # html_notebook:
  #   fig_caption: yes
  #   fig_height: 6
  #   number_sections: no
  #   theme: yeti
  #   toc: yes
    # toc_depth: 3
    # toc_float: yes

editor_options:
  chunk_output_type: inline
---



# Créer une application Web à l'aide de R et shiny

**Groupe de conception :**

* Gaëlle Genin
* Issam Khelladi
* Jean-Luc Lipatz
* Cédric Tassart
* Raymond Warnod

---

# Plan

* Introduction
* 1. Présentation rapide de Shiny
* 2. Un exemple pas à pas
* 3. Interaction entre l’interface et le programme
* 4. Gestion de la mise en page d’une application
* 5. Les fonctions du server
* 6. shinyfiles et interfaces dynamiques
* 7. Bon à savoir

---

# **1. Présentation rapide de Shiny**

* 1.1 Shiny
* 1.2 Principe du client-serveur
* 1.3 Structure d’une application Shiny
* 1.4 Création d’une application à partir de Rstudio
* 1.5 Lancement d’une application à partir de Rstudio

---

## 1.1 Shiny 

Shiny est un package R qui permet la création d'applications Web interactives.

---

## 1.2 Principe du client-serveur

Les utilisateurs manipulent l'interface.
Le serveur actualise l'affichage des résultats sur l'interface via du code R.

![](www/img/12_client_serveur.png)

---

## 1.3 Structure d’une application Shiny

Une application Shiny est composée de deux éléments :

* **ui **
la description de l'interface utilisateur (i.e. l'apparence de l'application)

* **server**
les instructions R nécessaires aux traitements et calculs.
Ces deux éléments peuvent simplement être répartis dans :

* un fichier (dit fichier unique) nommé `app.R`, 
* deux fichiers (dits fichiers multiples) nommés `ui.R` et `server.R`.

---

## 1.4 Création d’une application à partir de Rstudio

![](www/img/14 Creer appli.png)

---

## 1.5 Lancement d’une application à partir de Rstudio

![](www/img/15 lancer appli.png)

---

## **Exercice 1**

Dans RStudio, créer une nouvelle application Shiny

- `Application name`	: «Exo_1»,
- `Application type`	: multiple file (ui.r/server.r),
- `directory`	: votre répertoire «PGM».

Lancer l’application Shiny ainsi créée

----

### Exercice 1 - Correction

**VIDEO**

---


# **2. Un exemple pas à pas**

![](www/img/20 squelette appli.png)

---

## 2.1 Une application vide


```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)
```


```r
### Traitement dans l'environnement global, en dehors de l'application
# Chargement et traitement sur la table des populations de 1968 à 2015  

pop<- read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
  rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee<- names(pop %>% select_if(is.numeric))
```
Chargement des librairies et des données en dehors de l’application


```r
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
)

#serveur de calculs
server <- function(input, output) {
}

# Run the application 
shinyApp(ui = ui, server = server)
```

![](www/img/21 Apppli vide.PNG)

**A ce stade l'application est vide**

<details>
<summary markdown="span">**Voir le code**</summary>


```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)
library(DT)
### Traitement dans l'environnement global, en dehors de l'application
    # Chargement et traitement sur la table des populations de 1968 à 2015  

pop<- read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
                rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee<- names(pop %>% select_if(is.numeric))

#Début de l'application Shiny
#Interface
ui <- fluidPage( 

)

#serveur de calculs
server <- function(input, output) {

}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

## 2.2 Implémentation de l'`UI`


```r
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une année",
              choices= choix_annee)
  )
```

Ajout d’un titre et d’une liste déroulante dans l’interface (UI).

On remarque que les choix de la liste sont paramétrés à partir d’une variable
(choix_annee) définie en dehors de l’application.

![](www/img/22 Apppli vide.PNG)

**À ce stade, il n’ y a pas de calculs dans le server**

<details>
<summary markdown="span">**Voir le code**</summary>


```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)
library(DT)
### Traitement dans l'environnement global, en dehors de l'application
    # Chargement et traitement sur la table des populations de 1968 à 2015  

pop<- read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
                rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee<- names(pop %>% select_if(is.numeric))

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une année",
              choices= choix_annee)
  )

#serveur de calculs
server <- function(input, output) {

}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

## 2.3 Traitement des données


```r
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderTable({
    pop %>% group_by(REGION) %>%
      summarise_at(input$annee,sum,na.rm=T)
  })
}
# Run the application 
shinyApp(ui = ui, server = server)
```
Construction d’un `tableau` de la population par région dans le server.

![](www/img/22 Apppli vide.PNG)

**A ce stade, il n’est pas affiché dans l’interface de l’application**

<details>
<summary markdown="span">**Voir le code**</summary>


```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)


# Chargement et traitement sur la table 
#des populations de 68 à 2015  dans l'environnement global de R
pop<- read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
                rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee<- names(pop %>% select_if(is.numeric))

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une annee",
              choices= choix_annee)
)

#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderTable({
    pop %>% group_by(REGION) %>%
      summarise_at(input$annee,sum,na.rm=T)
  })
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>


---

## 2.4 Retour au `server`


```r
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une année",
              choices= choix_annee)
)

tableauOutput("tableau")
```

* Un titre
* une liste déroulante
* et un tableau modifiable par des sélections

![](www/img/23 Apppli vide.PNG)

**Nous avons une application réactive et pouvons afficher les données de :** `tableau`

<details>
<summary markdown="span">**Voir le code**</summary>

```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)

# Chargement et traitement sur la table 
#des populations de 68 à 2015  dans l'environnement global de R
pop<- read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
                rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee<- names(pop %>% select_if(is.numeric))

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une annee",
              choices= choix_annee)
  ,tableOutput("tableau")
)

#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderTable({
    pop %>% group_by(REGION) %>%
      summarise_at(input$annee,sum,na.rm=T)
  })
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

**L'objet input$annee est automatiquement modifié :**

![](www/img/25 Pas A Pas.PNG)

---

## **Exercice 2**

**Modifier l’application fournie (EXO2_original) en modifiant titres, couleur**

![](www/img/26 Exo2.PNG)

<details>
<summary markdown="span">**Correction Exo 2**</summary>


```r
# Chargement des Packages
library(shiny)
# install.packages("ggformula")
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  # Titre de l'application
  titlePanel("Titre"),
  
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",
              value=50,min=1,max=500),
  plotOutput("graphique")
)
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot(
    gf_histogram(~ rnorm(input$nbvaleurs),fill = "skyblue", color = "black",
                 title=titre_graphique,xlab = "") %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  )
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

# **3. Interaction entre l’interface et le programme**

* 3.1 Principe de fonctionnement
* 3.2 Circulation input – output
* 3.3 La réactivité
* 3.4 L’interface utilisateur (UI)
* 3.5 Le serveur
* 3.6 Les fonctions réactives
* 3.7 Le package DT
* 3.8 Le fonctionnement croisé

---

## 3.1 Principe de fonctionnement

**Dans l’ui, on trouve :**

* divers éléments de mise en page;
* des champs en entrée modifiables par l'utilisateur via l’objet `input`,
* des sorties dynamiques via l'objet `output`.
* Le lien entre les `input` et les `output` se fait dans la fonction server

![](www/img/31 elements UI.png)

---

**Dans le server :**

* On effectue des traitements à partir de paramètres d’entrée (`input$Annee`).
* À partir de ces résultats, les fonctions `render*()` alimentent les objets en sortie (`output$tableau`).
* Ceux-ci sont affichés dans l’interface (`ui`).

**NB:** `render*()` désigne toute fonction préfixée par render

---

## 3.2 Circulation input - output

![](www/img/32 circulation iput output.png)

---

## 3.3 La réactivité
Qu'est-ce que c'est ?

La réactivité d'une application, c'est sa capacité à mettre à jour ses éléments suite à l'action d'un utilisateur.

---

**Schéma de la réactivité**

![](www/img/33 Reactivite.PNG)

---

**La réactivité au plus court avec Shiny**

![](www/img/33 Reactivite au plus court.PNG)

---

## 3.4 L’interface utilisateur (UI)

L'`UI` (*User Interface*) est la vitrine de l'application. C'est une fenêtre dans laquelle l'utilisateur va «saisir» des informations : les `input`.
Les valeurs des `input` sont définies par manipulation de `widgets`. 

Les résultats (`output`) produits par l'application sont généralement affichés dans cette interface.

---

Pour permettre à l'utilisateur de saisir des données en entrée (un input), on utilise des widgets. Les plus courants sont :

Widgets                       | Rôle
----------------------------- | -------------
textInput                     | zone de saisie pour une variable texte
numericInput                  | zone de "saisie" pour une variable numérique
selectInput                   | liste déroulante
radioButtons                  | bouton radio
checkboxInput                 | case à cocher
checkboxGroupInput            | ensemble de cases à cocher
sliderInput                   | bouton à déplacer sur une barre (curseur)
actionButton                  | bouton pour effectuer une action
fileInput                     | bouton pour sélectionner un fichier
shinyFiles::shinyFilesButton  |	bouton pour sélectionner des fichiers
shinyFiles::shinySaveButton   | bouton pour sauvegarder des fichiers

[Pour en savoir plus](https://shiny.rstudio.com/reference/shiny/)

---

![](www/img/34 Widgets.PNG)

[Plus de widgets](https://shiny.rstudio.com/gallery/widget-gallery.html)

---

Les `widgets` sont des fonctions qui attendent plusieurs arguments. Les 2 premiers sont :

* `inputId` :	nom du widget
* `label`   :	titre du widget

**Exemple :** créer une liste déroulante (`selectInput`) nommée **select1** permettant d'effectuer un choix parmi les valeurs *a*, *b* et *c*.


```r
selectInput(
  inputId = "select1",
  label = "Veuillez faire un choix dans la liste",
  choices = c("choix 1" = "a", "choix 2" = "b", "choix 3" = "c")
)
```

![](www/img/37 select1.png)

---

## **Exercice 3**

Ajouter à cette application un widget de type textInput qui permet de modifier le titre du graphique.
![](www/img/39 Exo3.PNG)

<details>
<summary markdown="span">**Correction Exo 3**</summary>


```r
library(shiny)
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"
titre <-"EXO_3"
couleur <- "red"
valmin <- 5

ui <- fluidPage( 
  
  # Titre de l'application
  titlePanel(titre),
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",value=50,min=valmin,max=500),
  textInput("titreG",label="Entrer le titre du graphique",
            value=titre_graphique),
  
  plotOutput("graphique")
)

server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = couleur, color = "black",
                 title=input$titreG,xlab="") %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
 }

shinyApp(ui = ui, server = server)
```
</details>


---

## 3.5 Le serveur (`server`)

Le server contient le code `R` à exécuter pour effectuer tous les traitements nécessaires à l'application :

* Chargement de données
* Calculs
* Sélection, extraction
* Graphique, tableau...
* ...

---

## 3.6 Les fonctions réactives

On ne peut écrire dans le server qu’à partir de fonctions réactives (`render*`, `reactive`, `eventreactive`, `observers`)

---

Dans le fichier `server`, faire appel à une valeur réactive nécessite une fonction réactive

![](www/img/36 Fonctions reactives.PNG)

---

## 3.7 Le package DT

Certains packages permettent de disposer d’autres fonctions `render`.
Il y a par exemple le package `DT` qui améliore le rendu des tableaux.

---


```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)
```


```r
library(DT)
```


```r
# Chargement et traitement sur la table 
#des populations de 68 à 2015  dans l'environnement global de R
pop<- haven::read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
                rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee<- names(pop %>% select_if(is.numeric))
```




```r
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une annee",
              choices= choix_annee)
```


```r
  ,DTOutput("tableau")
)
```


```r
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderDT({
    pop %>% group_by(REGION) %>%
      summarise_at(input$annee,sum,na.rm=T)
  })
}
```


```r
# Run the application 
shinyApp(ui = ui, server = server)
```

![](www/img/38 DT.PNG)

<details>
<summary markdown="span">**Récupérer le code**</summary>

```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)

# Chargement et traitement sur la table 
#des populations de 68 à 2015  dans l'environnement global de R
pop<- read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
  rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee<- names(pop %>% select_if(is.numeric))

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une annee",
              choices= choix_annee),
  DTOutput("tableau")
)

#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderDT({
    pop %>% group_by(REGION) %>% 
      summarise_at(input$annee,sum,na.rm=T)
  })
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

Des options peuvent être ajoutées avec la fonction `datatable()`. On crée ici la fonction`formaterTableau`.


```r
# Fonction pour formatter les tableaux
formaterTableau<-function(tab){
  datatable(tab, 
            rownames = F, 
            width="100%",
            extensions = c('Buttons','ColReorder','FixedColumns')
            ,options = list(
              dom = 'Blfrtip',
              buttons = c("colvis",'csv', 'excel', 'pdf'),
              colReorder = TRUE,
              lengthMenu = list(c(5, 25,50, -1), c('5', '25',"50", 'Tout')),
              pageLength = 10
            )
   )
}
```
On fait appel à `formaterTableau()` dans le `renderDT` correspondant.


```r
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderDT({
    pop %>% group_by(REGION) %>%
      summarise_at(input$annee,sum,na.rm=T) %>% 
    formaterTableau
  })
}
```

![](www/img/38 DT2.PNG)

[Pour voir plus d'options, cf le site datatables](https://datatables.net/reference/option/)

---

<details>
<summary markdown="span">**Récupérer le code**</summary>

```r
#Chargement des Packages
library(shiny)
library(haven)
library(dplyr)
library(DT)

# Chargement et traitement sur la table 
#des populations de 68 à 2015  dans l'environnement global de R
pop<- read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
  rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))
# Vecteur des noms des colonnes numériques
choix_annee<- names(pop %>% select_if(is.numeric))

# On construit une fonction pour construire un tableau avec 
#des options de la fonction datatable du package DT
formaterTableau <- function(tab){
  datatable(tab, 
            extensions = c('Buttons','ColReorder','FixedColumns')
            ,options = list(
              dom = 'Blfrtip',
              buttons = c("colvis",'csv', 'excel', 'pdf'),
              colReorder = TRUE,
              lengthMenu = list(c(5, 15, -1), c('5', '15', 'Ensemble')),
              pageLength = 10
            )
  ) 
}
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  titlePanel("Population par Région"), #Titre
  #Liste déroulante pour le choix de l'année à représenter
  selectInput(inputId= "annee", label="Choisir une annee",
              choices= choix_annee),
  DTOutput("tableau")
)

#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$tableau <- renderDT({
    pop %>% group_by(REGION) %>% 
      summarise_at(input$annee,sum,na.rm=T) %>% 
      formaterTableau
  })
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

## 3.8 Le fonctionnement croisé

Les fonctions `render` côté **server** et `Output` côté **ui** vont de pair :

| ui                  | server
|---------------------|-------
| `plotOutput`        | `renderPlot`
| `textOutput`        | `renderText`
| `tableOutput`       | `renderTable`
| `uiOutput`          | `renderUI`
| `verbatimTextOutput`| `renderPrint`
| `imageOutput`       | `renderimage`

---

## **Exercices 4-5**

**À partir de l'appli précédente, créer l'appli "Exo_4"**
**Exo 4**

* Ajouter un widget de type selectInput qui permet de sélectionner la couleur du graphique,
* Ajouter un widget de type selectInput qui permet de sélectionner la couleur des bordures des barres de l'histogramme.

![](www/img/40 Exo4.png)

**Exo 5**

* Modifier les widgets précédemment créés pour afficher les couleurs en Français. 
![](www/img/40 Exo5.png)

<details>
<summary markdown="span">**Correction Exo 4 et 5**</summary>
<details>
<summary markdown="span">**Correction Exo 4**</summary>


```r
#Chargement des Packages
library(shiny)
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"
titre <-"EXO_4"
couleur <- "red"
couleur_bord <- "black"
valmin <- 5
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  
  # Titre de l'application
  titlePanel(titre),
  
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",value=50,min=valmin,max=500),
  textInput("titreGraphique",label="Entrer le titre du graphique",
            value=titre_graphique),
  selectInput("coul_hist","Couleur du graphique",
              choices = c("blue","red","orange","purple","green"),
              selected = couleur),
  selectInput("border_hist","Couleur des bordures",
              choices = c("black","blue","red","orange","purple","green"),
              selected="red"),
  
  
  plotOutput("graphique")
)
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = input$coul_hist, color = input$border_hist,xlab="",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
  
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

<details>
<summary markdown="span">**Correction Exo 5**</summary>


```r
#Chargement des Packages
library(shiny)
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"
titre <-"EXO_5"
couleur <- "red"
couleur_bord <- "black"
valmin <- 5
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  
  # Titre de l'application
  titlePanel(titre),
  
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",value=50,min=valmin,max=500),
  textInput("titreGraphique",label="Entrer le titre du graphique",
            value=titre_graphique),
  selectInput("coul_hist","Couleur du graphique",
              choices = c("bleu"="blue","rouge"="red","orange"="orange","violet"="purple","vert"="green"),
              selected = couleur),
  selectInput("border_hist","Couleur des bordures",
              choices = c("noir"="black","bleu"="blue","rouge"="red","orange"="orange","violet"="purple","vert"="green"),
              selected="red"),
  
  
  plotOutput("graphique")
)
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = input$coul_hist, color = input$border_hist,xlab="",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
  
}

# Run the application 
shinyApp(ui = ui, server = server)
```

</details>

---

# **4. Gestion de la mise en page d’une application avec** `shinydashboard`

* 4.1 `dashboardHeader()` 
* 4.2 `dashboardSidebar()` : créer des menus
* 4.3 `dashboardBody()` : afficher des données
* 4.4 Divers éléments de mise en page
* 4.5 La gestion des input

---

Le package `shinydashboard` permet de construire des applications avec une interface graphique de type tableau de bord.


---


```r
# app.R 
library(shiny)
library(shinydashboard)
ui <- dashboardPage(
  dashboardHeader(),
  dashboardSidebar(),
  dashboardBody()
)
server <- function(input, output) { }
shinyApp(ui, server)
```

---

![](www/img/41 dashboard vide.PNG)

---

## 4.1 dashboardheader()

On peut ensuite personnaliser chacune de ces 3 parties. 
La fonction `dashboardHeader()` permet de personnaliser l’en-tête de l’application.

### Ajouter un titre

Un en-tête de tableau de bord peut rester vide ou inclure :

* le titre de l’application,
* des éléments de menu déroulant à droite.

Ici, on ajoute le titre de l'application avec le paramètre **title() : « formation shiny »**


```r
# app.R 
library(shiny)
library(shinydashboard)
ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  dashboardSidebar(),
  dashboardBody()
)
server <- function(input, output) { }
shinyApp(ui, server)
```

---

![](www/img/41 dashboard titre.PNG)

---

### Ajouter un menu

Les menus déroulants sont générés par la fonction `dropdownMenu()`. Chacun des `dropdownMenu()` doit être renseigné avec un type d'élément correspondant :

* `messages`,
* `notifications`
* `tasks`

---

![](www/img/42 dashboard dropdownmenu.png)

---

Ci-dessous, on ajoute un menu déroulant de type "messages" qui comprend 3 messages.
La fonction `messageItem()` a besoin, au minimum, des paramètres :

- `from` 
- `message`  
Vous pouvez également contrôler l'**icône** et une chaîne de **temps de notification** (heure, date). 


```r
dashboardHeader(title = "formation shiny",
```

---


```r
dropdownMenu(type = "messages",
             messageItem(from = "message 1", message = "texte message 1"),
             messageItem(from = "message 2", message = "texte message 2",
                         icon = icon("question"),  time = "13:45"),
             messageItem(from = "message 3", message = "texte message 3",
                         icon = icon("life-ring"), time = "2014-12-01")
),
```

![](www/img/42 dashboard messages.PNG)

---

Ici, on ajoute un menu déroulant de type `notifications` qui comprend 3 notifications avec les paramètres :

- `text` 
- `icon`



```r
dropdownMenu(type = "notifications", badgeStatus = "warning",
             notificationItem(text = "2627 utilisateurs",
                              icon("users")),
             notificationItem(text = "187 articles",
                              icon("truck"),
                              status = "success"),
             notificationItem(text = "Problemes de connexion ce week-end",
                              icon = icon("exclamation-triangle"),
                              status = "warning")
)
```

---

![](www/img/42 dashboard notifications.png)

---

Ici, on ajoute un menu déroulant de type `tasks` qui comprend 4 tâches : chacune a un taux de réalisation différent. On ajoute les paramètres :

- `value` 
- `color`



```r
dropdownMenu(type = "tasks",
             badgeStatus = "success",
             taskItem(value = 5, color = "purple", "guide pédagogique"),
             taskItem(value = 90, color = "green", "exercice de formation"),
             taskItem(value = 60, color = "red", "exercice de synthese"),
             taskItem(value = 80, color = "yellow", "support de cours")
)
```

---

![](www/img/42 dashboard tasks.png)

---


<details>
<summary markdown="span">**Récupérer le code**</summary>

```r
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny",
                dropdownMenu(type = "messages",
                             messageItem(from = "message 1", message = "texte message 1"),
                             messageItem( from = "message 2", message = "texte message 2",
                                          icon = icon("clone"),  time = "13:45"),
                             messageItem(from = "message 3", message = "texte message 3",
                                         icon = icon("life-ring"), time = "2014-12-01") ),
                dropdownMenu(type = "notifications", badgeStatus = "warning",
                             notificationItem(text = "2627 utilisateurs",
                                              icon("users")),
                             notificationItem(text = "187 articles",
                                              icon("truck"),
                                              status = "success"),
                             notificationItem(text = "Problemes de connexion ce week-end",
                                              icon = icon("exclamation-triangle"),
                                              status = "warning") ),
                dropdownMenu(type = "tasks",
                             badgeStatus = "success",
                             taskItem(value = 5, color = "purple", "guide pédagogique"),
                             taskItem(value = 90, color = "green", "exercice de formation "),
                             taskItem(value = 60, color = "red", "exercice de synthese"),
                             taskItem(value = 80, color = "yellow", "support de cours") ) ),
  dashboardSidebar(),
  dashboardBody() )


server <- function(input, output) { }

shinyApp(ui, server)
```
</details>

---

Dans la plupart des cas, vous souhaiterez dynamiser le contenu de ces « boites à messages ». Cela signifie que le contenu HTML est généré côté serveur et envoyé au client pour le rendu.

* 1 - En amont, vous chargerez le fichier des messages (ici, `message.ods`). C'est un fichier avec différentes colonnes : au minimum de quoi compléter les paramètres `from` et `messages`. 


```r
messageData <- rio::import("donnees/messages.ods")
```

![](www/img/42 dashboard messages ods.PNG)

* 2 - Dans le code de l'interface utilisateur, vous utiliserez dropdownMenuOutput().

```r
dashboardHeader(
  title = "formation shiny",
  dropdownMenuOutput("messageMenu")
)
```

* 3 - Et côté serveur, vous générerez l’ensemble du menu dans une fonction de type renderMenu()

```r
server <- function(input, output) { 
  output$messageMenu <- renderMenu({
    msgs <- apply(messageData, 1, function(row) {
      messageItem(from = row[["from"]], message = row[["message"]],time =row[["time"]] )
    })
    dropdownMenu(type = "messages", .list = msgs)
  })
}
```

![](www/img/42 dashboard messages 2.PNG)


Pour plus de renseignements sur les autres types de menus déroulants (`notifications` et `tâches`), cf le [site shinydashboard (structure)](https://rstudio.github.io/shinydashboard/structure.htm)

<details>
<summary markdown="span">**Récupérer le code**</summary>

<details>
<summary markdown="span">**message 2**</summary>

```r
# app.R 
library(shiny)
library(shinydashboard)
library(rio)
# Adapter le setwd() si n?cessaire
messageData <- rio::import("donnees/messages.ods")

ui <- dashboardPage(
  dashboardHeader(
    title = "formation shiny",
    dropdownMenuOutput("messageMenu")
  ),
  dashboardSidebar(),
  dashboardBody()
)


server <- function(input, output) { 
  output$messageMenu <- renderMenu({
    msgs <- apply(messageData, 1, function(row) {
      messageItem(from = row[["from"]], message = row[["messages"]],time =row[["time"]] )
    })
    dropdownMenu(type = "messages", .list = msgs)
  })
}

shinyApp(ui, server)
```
</details>

<details>
<summary markdown="span">**Alimenter un menu. Exemple**</summary>
Sur les forums, on peut également trouver des exemples pour alimenter ces différents menus directement à partir de l'appli.


```r
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(
    dropdownMenuOutput("menu")
  ),
  dashboardSidebar(
    helpText("Ajouter une nouvelle tâche en cliquant sur le bouton ci-dessous"),
    actionButton("addItem", "Ajouter")
  ),
  dashboardBody()
)

server <- function(input, output, session) {
  tasks <-  reactiveValues(
    code = list(id = "code", value = 15, color = "aqua",
                text = "Refactoriser le code"),
    layout = list(id = "layout", value = 40, color = "green",
                  text = "Redéfinir la mise en page"),
    docs = list(id = "docs", value = 25, color = "red",
                text = "écrire la documentation")
  )
  
  # actually render the dropdownMenu
  output$menu <- renderMenu({
    items <- lapply(tasks, function(el) {
      taskItem(value = el$value, color = el$color, text = el$text)
    })
    dropdownMenu(
      type = "tasks", badgeStatus = "danger",
      .list = items
    )
  })
  
  observeEvent(input$addItem, {
    showModal(modalDialog(title = "Ajouter une nouvelle tâche",
                          textInput(paste0("id", input$addItem), "ID de la tâche"),
                          numericInput(paste0("val", input$addItem), "Tx de réalisation", 0),
                          selectInput(paste0("col", input$addItem), "Couleur",
                                      choices = c("rouge"="red", "jaune"="yellow", "bleu aqua"="aqua", "bleu"="blue",
                                                  "bleu clair"="light-blue", "vert"="green")
                          ),
                          textInput(paste0("text", input$addItem), "Libellé de la tâche"),
                          actionButton(paste0("go", input$addItem), "Ajouter"),
                          easyClose = TRUE, footer = NULL
    ))
    
    observeEvent(input[[paste0("go", input$addItem)]], {
      tasks[[paste0("id", input$addItem)]] <- list(
        id = input[[paste0("id", input$addItem)]],
        value = input[[paste0("val", input$addItem)]],
        color = input[[paste0("col", input$addItem)]],
        text = input[[paste0("text", input$addItem)]]
      )
      removeModal()
    })
  })
}

shinyApp(ui, server)
```

![](www/img/42 dashboard dropdownmenu Alimenter.png)

</details>

</details>
</details>

---

## **Exercice 6-1**
Réaliser cette application

![](www/img/43 Exo 6_1.png)

<details>
<summary markdown="span">**Correction Exo 6-1**</summary>

```r
library(shiny)
library(shinydashboard)
library(rio)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny", titleWidth = 300,
                  dropdownMenu(type = "messages",
                               messageItem(from = "message 1", message = "texte message 1"),
                               messageItem( from = "message 2", message = "texte message 2", 
                                            icon = icon("question"), time = "13:45"),
                               messageItem(from = "message 3", message = "texte message 3", 
                                           icon = icon("life-ring"), time = "2014-12-01")),
                  dropdownMenu(type = "notifications", badgeStatus = "warning",
                               notificationItem(text = "2627 utilisateurs", icon("users")),
                               notificationItem(text = "187 articles", icon("truck"), 
                                                status = "success"),
                               notificationItem(text = "Problèmes de connexion ce week-end", 
                                                icon = icon("exclamation-triangle"),
                                                status = "warning")),
                  dropdownMenu(type = "tasks",
                               badgeStatus = "success",
                               taskItem(value = 5, color = "purple", "guide pédagogique"),
                               taskItem(value = 90, color = "green", "exercice de formation "),
                               taskItem(value = 60, color = "red", "exercice de synthèse"),
                               taskItem(value = 80, color = "yellow", "support de cours"))),
  dashboardSidebar(width = 300,p("Rien dans le sideBar")),
  dashboardBody(p("Rien dans le body")))
server <- function(input, output) {observeEvent(input$quit, stopApp())}
shinyApp(ui, server)
```
</details>

---

## 4.2 `dashboardSidebar()` : créer des menus

La fonction `dashboardSidebar()` permet de personnaliser la barre latérale en ajoutant :

* des menus,
* des boutons ...
	

---

Nous pouvons donc ajouter du contenu à la barre latérale. Par exemple :

* des éléments de menu qui se comportent comme des onglets.
    - Page 1 : en cliquant sur l’onglet **Accueil**, on affiche la page correspondante.
![](www/img/44 dashboardsidebar.PNG)	
    - page 2 : On affiche la page **Table**.

On peut aussi ajouter des widgets, boutons, des messages, images ...

Ces éléments sont visibles quelle que soit la page sur laquelle vous travaillez.  
**Par exemple :** un bouton «Stop». 

<details>
<summary markdown="span">**Voir le code**</summary>

```r
## app.R ##
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil"),
      menuItem(tabName="onglet2", text ="Table")
      )),
  
  dashboardBody(
    tabItems(
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue")),
      tabItem(tabName="onglet2",
              h1("Bienvenue sur la page de données tabulées", style="color:blue"))
      )
  )
)

server <- function(input, output) { }

shinyApp(ui, server)
```
</details>

---

Dans le `dashboardSidebar`, on initie les différents onglets avec la fonction `menuItem`


```r
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil"),
      menuItem(tabName="onglet2", text ="Table")
    )),
```

Dans le «corps» de l’appli (`dashboardBody`), il faudra alors personnaliser chacun des `menuItem`.  
On ajoutera tout d’abord la fonction `tabItems()` dans lequel on définira 1 `tabItem` par `menuItem` initié ci-dessus.
La relation entre les 2 se fait par le `tabName`


```r
  dashboardBody(
    tabItems(
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue")),
      tabItem(tabName="onglet2",
              h1("Bienvenue sur la page de données tabulées", style="color:blue"))
      )
  )
```

On peut encore personnaliser : badges, couleur de badge...


```r
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil",
               badgeLabel = "Un", badgeColor = "red",icon = icon("bath")), br(),
      
      menuItem(tabName="onglet2", text ="Table",
               badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    )
  )
```

![](www/img/44 dashboardsidebar2.PNG)	

---

<details>
<summary markdown="span">**Voir le code**</summary>

```r
## app.R ##
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil", badgeLabel = "Un", badgeColor = "red",icon = icon("bath")), br(),
      menuItem(tabName="onglet2", text ="Table", badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    )
  ),
  
  dashboardBody(
    tabItems(
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue")),
      tabItem(tabName="onglet2",
              h1("Bienvenue sur la page de données tabulées", style="color:blue"))
      )
  )
)

server <- function(input, output) { }

shinyApp(ui, server)
```
</details>


---

## **Exercice 6-2**
Réaliser cette application. Consignes :

![](www/img/44 Exo 6_2.PNG)

<details>
<summary markdown="span">**Correction Exo 6-2**</summary>

```r
library(shiny)
library(shinydashboard)
library(rio)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny", titleWidth = 300,
                  dropdownMenu(type = "messages",
                               messageItem(from = "message 1", message = "texte message 1"),
                               messageItem( from = "message 2", message = "texte message 2", icon = icon("question"),  time = "13:45"),
                               messageItem(from = "message 3", message = "texte message 3", icon = icon("life-ring"), time = "2014-12-01")
                  ),
                  dropdownMenu(type = "notifications", badgeStatus = "warning",
                               notificationItem(text = "2627 utilisateurs", icon("users")),
                               notificationItem(text = "187 articles", icon("truck"), status = "success"),
                               notificationItem(text = "Problèmes de connexion ce week-end", icon = icon("exclamation-triangle"), status = "warning")
                  ),
                  dropdownMenu(type = "tasks",
                               badgeStatus = "success",
                               taskItem(value = 5, color = "purple", "guide pédagogique"),
                               taskItem(value = 90, color = "green", "exercice de formation "),
                               taskItem(value = 60, color = "red", "exercice de synthèse"),
                               taskItem(value = 80, color = "yellow", "support de cours")
                  )
  ),
  
  dashboardSidebar(width = 300,
                   sidebarMenu(
                     menuItem(tabName="onglet1", text="Onglet 1"),
                     tags$hr(),
                     
                     menuItem(tabName="onglet2", text ="Onglet 2", badgeLabel="Exercice", badgeColor="green", selected = TRUE),
                     tags$hr(),
                     menuItem(actionButton("quit", label = "Au revoir et à bientôt", width = "80%") )
                   )
  ),
  dashboardBody(
    tabItems(
      tabItem(tabName="onglet1",
              h1("Onglet 1 : Bienvenue !", style="color:blue")),
      tabItem(tabName="onglet2",
              h1("Onglet 2 : Bienvenue !", style="color:blue"))
      )
    
  )
)

server <- function(input, output) {
  observeEvent(input$quit,
               stopApp()
  )
}
shinyApp(ui, server)
```
</details>

---

## 4.3 `dashboardBody()` : afficher des données

Pour personnaliser le corps d’une appli, on peut ajouter différents types d’éléments:

* `box()`,
* `tabBox()`,
* `infoBox()`
	
### 4.31 les `box`


```r
  dashboardBody(
    tabItems(
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue"),
              
              box(id="box1", status = "primary", solidHeader = TRUE , background = "light-blue", title = "chargement des données",collapsible = TRUE,collapsed = F,
                  h4("Box() de sélection des données")
              ),
              box(id="box2", status = "success", solidHeader = TRUE , title = "tables de données", width = 12, collapsible = TRUE, collapsed = FALSE,
                  h4("Box() d'affichage des données")
              )
              )))
```

![](www/img/45 box.PNG)


<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil", badgeLabel = "Un", badgeColor = "red",icon = icon("bath")),
      br(),
      menuItem(tabName="onglet2", text ="Table", badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    )
  ),
  
  dashboardBody(
    tabItems(
      
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue"),
              
              box(id="box1", status = "primary", solidHeader = TRUE , background = "light-blue", title = "chargement des données",collapsible = TRUE,collapsed = F,
                  h4("Box() de sélection des données")
              ),
              
              box(id="box2", status = "success", solidHeader = TRUE , title = "tables de données", width = 12, collapsible = TRUE, collapsed = FALSE,
                  h4("Box() d'affichage des données")
              )
      ),
      
      tabItem(tabName="onglet2",
              h1("Bienvenue sur la page de données tabulées", style="color:blue")
      )
      
    )
  )
)

server <- function(input, output) { }

shinyApp(ui, server)
```
</details>

---

**Les principaux paramètres des** `box()`

|Paramètre        | Paramètres          | Explications                                             |
|-------------    |---------------------|-----------------------------------------------------------
|`status`         | primary (bleu), success(vert), info(bleu), warning (orange), danger (rouge) |permet de définir une couleur de box() et représenter un état/statut                              |
|`solidHeader`    | TRUE, FALSE         | Faut-il afficher la barre d’en-tête, avec la couleur ?   |
|`background`     | red, yellow, aqua, blue, light-blue, green, navy ... | couleur de l’arrière plan                                                                                                    |
|`title`          | titre (optionnel)   | titre à afficher dans l’en-tête                          |
|`Width`          |                     | la taille sur une grille de 12 unités                    |
|`collapsible`    | TRUE, FALSE         | Si TRUE, affiche un bouton en haut à droite permettant à l’utilisateur de réduire la zone                                                                   |
|`collapsed`      | TRUE, FALSE         |  Si TRUE, la `box()` est déployée                        |

---

Les sorties peuvent également être intégrées dans des `box()`

![](www/img/45 box sorties.PNG)

---

**dans l'**`UI`


```r
box(id="box2", status = "success", solidHeader = TRUE , 
    title = "Table de données", width = 12, collapsible = TRUE, collapsed = FALSE,
    DTOutput("tableau")
    )
```

**Dans le** `server`**, pas de changement**


```r
output$tableau <- renderDT({ 
  pop %>%  group_by(REGION)  %>% summarise_at (input$ANNEE, sum,na.rm=TRUE) 
})
```

**Dans l'environnement global**

```r
library(DT)
library(shiny)
library(shinydashboard)	

pop <- readRDS("donnees/poplegale_6815.RDS")

formaterTableau<-function(tab){
  datatable(tab, 
            rownames = F, width="100%",
            extensions = c('Buttons','ColReorder','FixedColumns')
            ,options = list(
              dom = 'Blfrtip',
              buttons = c("colvis",'csv', 'excel', 'pdf'),
              colReorder = TRUE,
              lengthMenu = list(c(5, 25,50, -1), c('5', '25',"50", 'Tout')),
              pageLength = 10
            )
   )
}
```

<details>
<summary markdown="span">**Voir le code**</summary>


```r
library(shiny)
library(shinydashboard)
library(dplyr)
library(ggformula)
library(DT)

pop <- readRDS("donnees/poplegale_6815.RDS")
choix_annee <- names(pop %>% select_if(is.numeric))

formaterTableau<-function(tab){
  datatable(tab, 
            rownames = F, width="100%",
            extensions = c('Buttons','ColReorder','FixedColumns')
            ,options = list(
              dom = 'Blfrtip',
              buttons = c("colvis",'csv', 'excel', 'pdf'),
              colReorder = TRUE,
              lengthMenu = list(c(5, 25,50, -1), c('5', '25',"50", 'Tout')),
              pageLength = 10
            )
   )
}

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil",badgeLabel ="un" ,badgeColor ="green",
               icon = icon("bath") ),
      menuItem(tabName="onglet2", text ="Table",badgeLabel ="deux" ,badgeColor ="red",
               icon=icon("blind"))
    )
  ),
  dashboardBody(
      tabItems(
        tabItem( tabName="onglet1",
            box(id="box1", status = "primary", solidHeader = T , background = "light-blue", 
              title = "Choix du millesime",collapsible = TRUE,collapsed = FALSE,
            selectInput(inputId= "ANNEE", label="Choisir une annee",
                          choices= choix_annee)
                 ),
            box(id="box2", status = "success", solidHeader = TRUE , 
                title = "Table de données", width = 12, 
                collapsible = TRUE, collapsed = FALSE,
                DTOutput("tableau"))
            ),
          tabItem (tabName="onglet2",h1("Bienvenue sur la page des données")) 
        )
  )
)

server <- function(input, output){ 
  output$tableau <- renderDT({ 
    pop %>%  group_by(REGION)  %>% 
      summarise_at (input$ANNEE,sum,na.rm=T) %>% 
      formaterTableau
  })
  
  }
  
shinyApp(ui, server)
```
</details>

---

## **Exercice 6-3**

**Réaliser l'interface suivante**

![](www/img/45 Exo 6_3.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>


```r
library(shiny)
library(shinydashboard)
library(rio)
ui <- dashboardPage(
  dashboardHeader(title = "formation shiny", titleWidth = 300,
                  dropdownMenu(type = "messages",
                               messageItem(from = "message 1", message = "texte message 1"),
                               messageItem( from = "message 2", message = "texte message 2", icon = icon("question"),  time = "13:45"),
                               messageItem(from = "message 3", message = "texte message 3", icon = icon("life-ring"), time = "2014-12-01")),
                  dropdownMenu(type = "notifications", badgeStatus = "warning",
                               notificationItem(text = "2627 utilisateurs", icon("users")),
                               notificationItem(text = "187 articles", icon("truck"), status = "success"),
                               notificationItem(text = "Problèmes de connexion ce week-end", icon = icon("exclamation-triangle"), status = "warning")),
                  dropdownMenu(type = "tasks",
                               badgeStatus = "success",
                               taskItem(value = 5, color = "purple", "guide pédagogique"),
                               taskItem(value = 90, color = "green", "exercice de formation "),
                               taskItem(value = 60, color = "red", "exercice de synthèse"),
                               taskItem(value = 80, color = "yellow", "support de cours"))
  ),
  dashboardSidebar(width = 300,
                   sidebarMenu(
                     menuItem(tabName="onglet1", text="Onglet 1"),
                     tags$hr(),
                     menuItem(tabName="onglet2", text ="Onglet 2", badgeLabel="Exercice", badgeColor="green", selected = TRUE),
                     tags$hr(),
                     menuItem(actionButton("quit", label = "Au revoir et à bientôt", width = "80%") )
                   )),
  dashboardBody(
    tabItems(
      tabItem(tabName= "onglet1",
              h1("Bienvenue sur l'onglet 1", style="color:blue")),
      tabItem(tabName= "onglet2",
              box(id="box0", status = "danger", solidHeader = TRUE , title = "Exercice de mise en page", width = 12, collapsible = TRUE, collapsed = TRUE,
                  h3("Box numéro 1", style="color : darkBlue"),
                  h4("1 - Libellés :", style="color : blue"),
                  h5("- libellé 1a ...", style="color : steelBlue"),
                  h4("2 - Libellés :", style="color : blue"),
                  h5("- libellé 2a ...", style="color : steelBlue"),
                  h4("3 - Libellés :", style="color : blue"),
                  h5("- libellé 3a ...", style="color : steelBlue"),
                  h4("4 - Libellés :", style="color : blue"),
                  h5("- libellé 4a ...", style="color : steelBlue"),
                  h5("- libellé 4b ...", style="color : steelBlue") ),
              box(id="boxInfo", status = "warning", solidHeader = FALSE, width = 12,
                  h4("5 - Libellés :", style="color : blue"),
                  h5("- libellé 5a ...", style="color : steelBlue"),
                  h5("- libellé 5b ...", style="color : steelBlue"),
                  h5("- libellé 5c ...", style="color : steelBlue"),
                  p("(ceci est un paragraphe inutile)")
              ),
              box(id="box3", status = "success", solidHeader = TRUE , width = 6, title = "Critères de filtre 1/4",  collapsible = TRUE, collapsed = TRUE,
                  selectInput(inputId = "chxTer", label = "Choisir votre zonage",
                              choices=c("Sélectionner un territoire","France entière", "Régions"="REGION","Départements"="D"), multiple = F),
                  
                  selectInput(inputId = "reg", label = "Sélectionner  la (ou les) région(s) de votre choix",
                              choices=c("Hauts de France","Aquitaine","Normandie","Ile de France"), multiple = T),
                  
                  selectInput(inputId = "dep", label = "Sélectionner  le (ou les) département(s) de votre choix",
                              choices=c("Somme","Aisne","Oise","Nord","Pas-de-Calais","Haute-garonne","Essonne","Ardèche","Calvados"), multiple = T),
                  radioButtons(inputId = "button1", label = "choisir une commune ",choices=c("Paris","Lille","Lyon","Marseille","Toulouse","Bordeaux","Cayeux-sur-mer")) ),
              box(id="box4", status = "success", solidHeader = TRUE , title = "Critères de filtre 2/4", width = 6, collapsible = TRUE, collapsed = TRUE,
                  h3("box 4 (fermée)")  ),
              box(id="box5", status = "success", solidHeader = TRUE , title = "Critères de filtre 3/4", width = 6, collapsible = TRUE, collapsed = TRUE,
                  h3("box 5 (fermée)") ),
              box(id="box6", status = "primary", solidHeader = TRUE , title = "Critères de filtre 4/4", width = 12, collapsible = TRUE, collapsed = TRUE,
                  h3("box 6 (fermée)") )
      )
    )
  )
)

server <- function(input, output) {
  observeEvent(input$quit,
               stopApp()
  )
}
shinyApp(ui, server)
```
</details>

### 4.32 les `tabBox`

Pour créer une tabBox, 2 mots clés :

* `tabBox` et `tabPanel` pour définir les différents panneaux/onglets de la boîte.

Ici, on va créer, dans le panneau **Table**, une boite avec 5 onglets :

* « Statistiques »,
* «  table de données »,
* « Graphiques »,
* « Cartes »,
* « Info diverses ... »

---

![](www/img/46 tabbox.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>

```r
## app.R ##
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil", badgeLabel = "Un", badgeColor = "red",icon = icon("bath")),
      br(),
      menuItem(tabName="onglet2", text ="Table", badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    )
  ),
  
  dashboardBody(
    tabItems(
      
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue"),
              
              box(id="box1", status = "primary", solidHeader = TRUE , background = "light-blue", title = "chargement des données",collapsible = TRUE,collapsed = F,
                  h4("Box() de sélection des données")
              ),
              
              box(id="box2", status = "success", solidHeader = TRUE , title = "tables de données", width = 12, collapsible = TRUE, collapsed = FALSE,
                  h4("Box() d'affichage des données")
              )
      ),
      
      tabItem(tabName="onglet2",
              h1("Bienvenue sur la page de données tabulées", style="color:blue"),
              tabBox(
                title = "Donnees filtrees", width = 12, id = "tabBox1",
                
                tabPanel(title = "Statistiques"
                ),
                
                tabPanel(title = "table de donnees" 
                ),
                
                tabPanel(title = "Graphiques"
                ),
                
                tabPanel(title = "Cartes"
                ),
                
                tabPanel(title = "Informations diverses (Variables & Concepts)"
                )
              )
      )
      
    )
  )
)

server <- function(input, output) { }

shinyApp(ui, server)
```
</details>

---

A l’instar de ce qu’on fait pour les box(), on va ajouter différents widgets et sorties à afficher.

Ici, dans le panneau **« Statistiques »**, on attend une sortie de type tableau (`filtreStats`) qui affichera des statistiques selon les critères définies dans la boite du dessus (**« Critères de filtre »**).


L'onglet 'table de données" affiche tous les enregistrements respectant les critères de ces filtres.

Les onglets **"Graphiques"** et **"cartes"** accueilleront des objets de types `plot`.
L'onglet `Info diverses` lit et affiche une table : un dictionnaire des variables.

Voilà le code adéquat avec la `tabBox()`, les 5 `tabPanel()` et les sorties attendues.



```r
tabBox(
  title = "Donnees filtrees", width = 12, id = "tabBox1",
  
  tabPanel(title = "Statistiques", 
           DTOutput("filtreStats")
  ),
  
  tabPanel(title = "table de donnees", 
           h4("Table des donnees filtrees", style="color: SteelBlue"),
           hr(),
           DTOutput("filtreTable")
  ),
  
  tabPanel(title = "Graphiques", 
           plotOutput("graph")
  ),
  
  tabPanel(title = "Cartes", 
           plotOutput("Carto") 
  ),
  
  tabPanel(title = "Informations diverses (Variables & Concepts)",
           DTOutput("infoDiv") 
  )
)
```


## **Exercice 6-4**

![](www/img/46 Exo 6_4.PNG)

<details>
<summary markdown="span">**Correction Exo 6-4**</summary>

```r
library(shiny)
library(shinydashboard)
library(rio)
ui <- dashboardPage(
  dashboardHeader(title = "formation shiny", titleWidth = 300,
                  dropdownMenu(type = "messages",
                               messageItem(from = "message 1", message = "texte message 1"),
                               messageItem( from = "message 2", message = "texte message 2", icon = icon("question"),  time = "13:45"),
                               messageItem(from = "message 3", message = "texte message 3", icon = icon("life-ring"), time = "2014-12-01") ),
                  dropdownMenu(type = "notifications", badgeStatus = "warning",
                               notificationItem(text = "2627 utilisateurs", icon("users")),
                               notificationItem(text = "187 articles", icon("truck"), status = "success"),
                               notificationItem(text = "Problèmes de connexion ce week-end", icon = icon("exclamation-triangle"), status = "warning")),
                  dropdownMenu(type = "tasks",
                               badgeStatus = "success",
                               taskItem(value = 5, color = "purple", "guide pédagogique"),
                               taskItem(value = 90, color = "green", "exercice de formation "),
                               taskItem(value = 60, color = "red", "exercice de synthèse"),
                               taskItem(value = 80, color = "yellow", "support de cours")) ),
  dashboardSidebar(width = 300,
                   sidebarMenu(
                     menuItem(tabName="onglet1", text="Onglet 1"), tags$hr(),
                     menuItem(tabName="onglet2", text ="Onglet 2", badgeLabel="Exercice", badgeColor="green", selected = TRUE), tags$hr(),
                     menuItem(actionButton("quit", label = "Au revoir et à bientôt", width = "80%") ) ) ),
  dashboardBody(
    tabItems(
      tabItem(tabName= "onglet1", h1("Bienvenue sur l'onglet 1", style="color:blue") ),
      tabItem(tabName= "onglet2",
              box(id="box0", status = "danger", solidHeader = TRUE , title = "Exercice de mise en page", width = 12, collapsible = TRUE, collapsed = TRUE,
                  h3("Box numéro 1", style="color : darkBlue"),
                  h4("1 - Libellés :", style="color : blue"),
                  h5("- libellé 1a ...", style="color : steelBlue"),
                  h4("2 - Libellés :", style="color : blue"),
                  h5("- libellé 2a ...", style="color : steelBlue"),
                  h4("3 - Libellés :", style="color : blue"),
                  h5("- libellé 3a ...", style="color : steelBlue"),
                  h4("4 - Libellés :", style="color : blue"),
                  h5("- libellé 4a ...", style="color : steelBlue"),
                  h5("- libellé 4b ...", style="color : steelBlue") ),
              box(id="boxInfo", status = "warning", solidHeader = FALSE, width = 12, collapsible = TRUE, collapsed = TRUE,
                  h4("5 - Libellés :", style="color : blue"),
                  h5("- libellé 5a ...", style="color : steelBlue"),
                  h5("- libellé 5b ...", style="color : steelBlue"),
                  h5("- libellé 5c ...", style="color : steelBlue"),
                  p("(ceci est un paragraphe inutile)") ),
              box(id="box3", status = "success", solidHeader = TRUE , width = 6, title = "Critères de filtre 1/4",  collapsible = TRUE, collapsed = TRUE,
                  selectInput(inputId = "chxTer", label = "Choisir votre zonage", choices=c("Sélectionner un territoire","France entière", "Régions"="REGION","Départements"="D"), multiple = F),
                  selectInput(inputId = "reg", label = "Sélectionner  la (ou les) région(s) de votre choix", choices=c("Hauts de France","Aquitaine","Normandie","Ile de France"), multiple = T),
                  selectInput(inputId = "dep", label = "Sélectionner  le (ou les) département(s) de votre choix", choices=c("Somme","Aisne","Oise","Nord","Pas-de-Calais","Haute-garonne","Essonne","Ardèche","Calvados"), multiple = T),
                  radioButtons(inputId = "button1", label = "choisir une commune ",choices=c("Paris","Lille","Lyon","Marseille","Toulouse","Bordeaux","Cayeux-sur-mer"))),
              box(id="box4", status = "success", solidHeader = TRUE , title = "Critères de filtre 2/4", width = 6, collapsible = TRUE, collapsed = TRUE, h3("box 4 (fermée)")),
              box(id="box5", status = "success", solidHeader = TRUE , title = "Critères de filtre 3/4", width = 6, collapsible = TRUE, collapsed = TRUE, h3("box 5 (fermée)")),
              box(id="box6", status = "primary", solidHeader = TRUE , title = "Critères de filtre 4/4", width = 12, collapsible = TRUE, collapsed = TRUE, h3("box 6 (fermée)")),
              tabBox(selected = "Panneau 2", width = 12, id = "tabBox1", 
                     tabPanel(title = "Panneau 1"),
                     tabPanel(title = "Panneau 2",h4("Panneau 2", style="color: SteelBlue") ),
                     tabPanel(title = "Panneau 3"),
                     tabPanel(title = "Panneau 4" ),
                     tabPanel(title = "Panneau 5")) ) ) ) )
server <- function(input, output) { observeEvent(input$quit, stopApp())}
shinyApp(ui, server)
```
</details>

### 4.33 les `infoBox()`

Elles permettent d’afficher des valeurs numériques ou textuelles simples, avec une icône. Dans l'exemple précédent, on affiche 3 `infoBox()` permettant de visualiser :

* la date du jour,
* l'IDEP de l'utilisateur,
* une boîte permettant de lancer la page web "shinydashboard"

Ces boîtes sont visibles à partir de n’importe quelle page de l’application.

---

l’`infoBox()` peut être dynamique ou non. On la définit dans l'`UI` si elle est statique ou dans le `server` au sein d'un `renderInfoBox()` si elle est dynamique.

**Non dynamique :**

* `infoBox()`,
* les valeurs à afficher sont mises dans le paramètre `value =`


```r
dashboardBody(
  fluidRow(
    infoBox(title = "Date", value = Sys.Date(), icon = icon("credit-card")),
    infoBox(title = "IdepUser", value = toupper(Sys.getenv("USERNAME")), icon = icon("credit-card")),
    infoBoxOutput(outputId = "siteInternet", width = 4)
  ),
```

**Dynamique (Exemple : siteInternet):**
On utilise :

* `infoBoxOutput()` dans la partie `UI` où on définit un `outputId`,
* on définit la sortie de type `renderInfoBox()` dans le `server` où on intègre l'`infoBox()`.


```r
 output$siteInternet <- renderInfoBox({
    infoBox(
      " ",value="shinydashboard : le site", icon = icon("thumbs-up"),
      color = "blue", fill = TRUE, href = "https://rstudio.github.io/shinydashboard/index.html"
    )
```

---

![](www/img/47 infobox.PNG)


<details>
<summary markdown="span">**Voir le code**</summary>

```r
## app.R ##
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil", badgeLabel = "Un", badgeColor = "red",icon = icon("bath")),
      br(),
      menuItem(tabName="onglet2", text ="Table", badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    )
  ),
  
  dashboardBody(
    fluidRow(
      infoBox(title = "Date", value = Sys.Date(), icon = icon("credit-card")),
      infoBox(title = "IdepUser", value = toupper(Sys.getenv("USERNAME")), icon = icon("credit-card")),
      infoBoxOutput(outputId = "siteInternet", width = 4)
    ),
    tabItems(
      
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue"),
              
              box(id="box1", status = "primary", solidHeader = TRUE , background = "light-blue", title = "chargement des données",collapsible = TRUE,collapsed = F,
                  h4("Box() de sélection des données")
              ),
              
              box(id="box2", status = "success", solidHeader = TRUE , title = "tables de données", width = 12, collapsible = TRUE, collapsed = FALSE,
                  h4("Box() d'affichage des données")
              )
      ),
      
      tabItem(tabName="onglet2",
              h1("Bienvenue sur la page de données tabulées", style="color:blue"),
              tabBox(
                title = "Donnees filtrees", width = 12, id = "tabBox1",
                
                tabPanel(title = "Statistiques"
                ),
                
                tabPanel(title = "table de donnees" 
                ),
                
                tabPanel(title = "Graphiques"
                ),
                
                tabPanel(title = "Cartes"
                ),
                
                tabPanel(title = "Informations diverses (Variables & Concepts)"
                )
              )
      )
      
    )
  )
)

server <- function(input, output) {
    output$siteInternet <- renderInfoBox({
    infoBox(
      " ",value="shinydashboard : le site", icon = icon("thumbs-up"),
      color = "blue", fill = TRUE, href = "https://rstudio.github.io/shinydashboard/index.html"
    )
  })
}

shinyApp(ui, server)
```
</details>
---

## **Exercice 6-5**

**Reproduire cette appli. Consignes :**

![](www/img/47 Exo 6_5.PNG)

<details>
<summary markdown="span">**Correction Exo 6-5**</summary>

```r
library(shinydashboard)
library(rio)
ui <- dashboardPage(
  dashboardHeader(title = "formation shiny", titleWidth = 300,
                  dropdownMenu(type = "messages",
                               messageItem(from = "message 1", message = "texte message 1"),
                               messageItem( from = "message 2", message = "texte message 2", icon = icon("question"),  time = "13:45"),
                               messageItem(from = "message 3", message = "texte message 3", icon = icon("life-ring"), time = "2014-12-01") ),
                  dropdownMenu(type = "notifications", badgeStatus = "warning",
                               notificationItem(text = "2627 utilisateurs", icon("users")),
                               notificationItem(text = "187 articles", icon("truck"), status = "success"),
                               notificationItem(text = "Problèmes de connexion ce week-end", icon = icon("exclamation-triangle"), status = "warning")),
                  dropdownMenu(type = "tasks",
                               badgeStatus = "success",
                               taskItem(value = 5, color = "purple", "guide pédagogique"),
                               taskItem(value = 90, color = "green", "exercice de formation "),
                               taskItem(value = 60, color = "red", "exercice de synthèse"),
                               taskItem(value = 80, color = "yellow", "support de cours")) ),
  dashboardSidebar(width = 300,
                   sidebarMenu(
                                          menuItem(tabName="onglet1", text="Onglet 1"), tags$hr(),
                                          menuItem(tabName="onglet2", text ="Onglet 2", badgeLabel="Exercice", badgeColor="green", selected = TRUE), tags$hr(),
                                          menuItem(actionButton("quit", label = "Au revoir et à bientôt", width = "80%") ) ) ),
   dashboardBody(
    infoBox(title = "Nous sommes le : ", value = Sys.Date(), width = 6, icon = icon("credit-card")),
    infoBoxOutput(outputId = "siteInternet", width = 6),
        tabItems(
      tabItem(tabName= "onglet1", h1("Bienvenue sur l'onglet 1", style="color:blue") ),
            tabItem(tabName= "onglet2",
              box(id="box0", status = "danger", solidHeader = TRUE , title = "Exercice de mise en page", width = 12, collapsible = TRUE, collapsed = TRUE,
                  h3("Box numéro 1", style="color : darkBlue"),
                           h4("1 - Libellés :", style="color : blue"),
                           h5("- libellé 1a ...", style="color : steelBlue"),
                           h4("2 - Libellés :", style="color : blue"),
                           h5("- libellé 2a ...", style="color : steelBlue"),
                           h4("3 - Libellés :", style="color : blue"),
                           h5("- libellé 3a ...", style="color : steelBlue"),
                           h4("4 - Libellés :", style="color : blue"),
                           h5("- libellé 4a ...", style="color : steelBlue"),
                           h5("- libellé 4b ...", style="color : steelBlue") ),
                         box(id="boxInfo", status = "warning", solidHeader = FALSE, width = 12, collapsible = TRUE, collapsed = TRUE,
                             h4("5 - Libellés :", style="color : blue"),
                             h5("- libellé 5a ...", style="color : steelBlue"),
                             h5("- libellé 5b ...", style="color : steelBlue"),
                             h5("- libellé 5c ...", style="color : steelBlue"),
                             p("(ceci est un paragraphe inutile)") ),
                            box(id="box3", status = "success", solidHeader = TRUE , width = 6, title = "Critères de filtre 1/4",  collapsible = TRUE, collapsed = TRUE,
                                             selectInput(inputId = "chxTer", label = "Choisir votre zonage", choices=c("Sélectionner un territoire","France entière", "Régions"="REGION","Départements"="D"), multiple = F),
                                                      selectInput(inputId = "reg", label = "Sélectionner  la (ou les) région(s) de votre choix", choices=c("Hauts de France","Aquitaine","Normandie","Ile de France"), multiple = T),
                                                      selectInput(inputId = "dep", label = "Sélectionner  le (ou les) département(s) de votre choix", choices=c("Somme","Aisne","Oise","Nord","Pas-de-Calais","Haute-garonne","Essonne","Ardèche","Calvados"), multiple = T),
                           radioButtons(inputId = "button1", label = "choisir une commune ",choices=c("Paris","Lille","Lyon","Marseille","Toulouse","Bordeaux","Cayeux-sur-mer"))),
                                    box(id="box4", status = "success", solidHeader = TRUE , title = "Critères de filtre 2/4", width = 6, collapsible = TRUE, collapsed = TRUE, h3("box 4 (fermée)")),
                  box(id="box5", status = "success", solidHeader = TRUE , title = "Critères de filtre 3/4", width = 6, collapsible = TRUE, collapsed = TRUE, h3("box 5 (fermée)")),
                  box(id="box6", status = "primary", solidHeader = TRUE , title = "Critères de filtre 4/4", width = 12, collapsible = TRUE, collapsed = TRUE, h3("box 6 (fermée)")),
              tabBox(selected = "Panneau 2", width = 12, id = "tabBox1", 
                     tabPanel(title = "Panneau 1"),
                     tabPanel(title = "Panneau 2",h4("Panneau 2", style="color: SteelBlue") ),
                     tabPanel(title = "Panneau 3"),
                     tabPanel(title = "Panneau 4" ),
                     tabPanel(title = "Panneau 5")) ) ) ) )
server <- function(input, output) {  
  output$siteInternet <- renderInfoBox({infoBox("shinydashboard",value="Accéder au site web", icon = icon("thumbs-up"),color = "blue", fill = TRUE, href = "https://rstudio.github.io/shinydashboard/index.html")})
  observeEvent(input$quit, stopApp() )
}
shinyApp(ui, server)
```
</details>

## 4.4 Divers éléments de mise en page

### 4.41 column(), fluidRow(),

Pour qu’une application s’affiche de manière agréable sur un écran de PC, une tablette ou un téléphone, il faut ajuster l’affichage. Cette technique fait partie de ce qu'on appelle le `Responsive design`.

Pour implémenter cette idée avec Shiny on découpe la page en lignes (`fluidRow`) et en colonnes (`column`).

* **FluidRow()**  
Fonction pour créer des mises en page fluides. Une mise en page fluide se compose de lignes qui incluent à leur tour des colonnes. Les rangées existent dans le but de s'assurer que leurs éléments apparaissent sur la même ligne (si le navigateur a une largeur adéquate). Les colonnes ont pour but de définir la quantité d'espace horizontal dans une grille large de 12 unités occupée par ses éléments. Les pages fluides redimensionnent leurs composants en temps réel pour couvrir toute la largeur disponible du navigateur.

* **Column()**  
`Width :` Le premier paramètre de la fonction column () est sa largeur (sur un total de 12 colonnes).  
`Offset :` Il est également possible de décaler la position des colonnes pour obtenir un contrôle plus précis de la localisation des éléments de l'interface utilisateur. Vous pouvez déplacer les colonnes vers la droite en ajoutant le paramètre offset à la fonction column (). Chaque unité de décalage augmente la marge gauche d'une colonne, d'une colonne entière.

* **Un 1er exemple simple**

    - Exécuter le code suivant dans `RStudio`;
    - Ajuster la largeur de la fenêtre pour voir comment la page s’adapte;
    - Noter qu’en plus d’empiler les colonnes d’une même ligne, le texte est également ajusté pour éviter l’ajout d’une barre de défilement horizontale.


```r
library(shiny)
ui <- fluidPage(
  title = "Hello Shiny!",
  fluidRow(
    column(width = 4,
           "Colonne 1 --> colum(width = 4)"
    ),
    column(width = 4, 
           "Colonne 2 --> colum(width = 4) ... Un peu de texte pour passer sur 2 lignes"
    ),
    column(width = 4, 
           "Colonne 3 --> colum(width = 4, offset=3) ... mais on rajoute un peu plus de texte pour illustrer le retour automatique à la ligne.
           Profitez-en pour redimensionner votre fenêtre. C'est magique, c'est responsaive design !"
    )
    )
  )

shinyApp(ui, server = function(input, output) { })
```

* **Un 2ème exemple plus élaboré**

![](www/img/48 column 1.PNG)


```r
tabPanel(title="divers",
         h2("colonne 1", style="color:blue"),
         h3("colonne 2", style="color:green"),
         h4("Colonne 3",style="color:red")
)
```

Par défaut, « les colonnes » se positionnent les unes en dessous des autres

<details>
<summary markdown="span">**Voir le code**</summary>

```r
## app.R ##
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet2", text ="Table", badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    )
  ),
  
  dashboardBody(
    fluidRow(
      infoBox(title = "Date", value = Sys.Date(), icon = icon("credit-card")),
      infoBox(title = "IdepUser", value = toupper(Sys.getenv("USERNAME")), icon = icon("credit-card")),
      infoBoxOutput(outputId = "siteInternet", width = 4)
    ),
    tabItems(
    
      
      tabItem(tabName="onglet2",
              tabBox(width = 10, id = "tabBox1",selected = "divers",
                     
                tabPanel(title = "stat"
                ),
                
                tabPanel(title = "graph" 
                ),
                
                tabPanel(title = "carte"
                ),
                
                tabPanel(title="divers",
                         h2("colonne 1", style="color:blue"),
                         h3("colonne 2", style="color:green"),
                         h4("Colonne 3",style="color:red")
                )
              )
      )
      
    )
  )
)

server <- function(input, output) {
  output$siteInternet <- renderInfoBox({
    infoBox(
      " ",value="shinydashboard : le site", icon = icon("thumbs-up"),
      color = "blue", fill = TRUE, href = "https://rstudio.github.io/shinydashboard/index.html"
    )
  })
}
shinyApp(ui, server)
```
</details>

La fonction `column` permet de fixer la taille des éléments sur une grille de 12. En attribuant une taille de 4 à chacun des trois éléments, ces derniers sont disposés côte à côte

![](www/img/48 column 2.PNG)


```r
tabPanel(title="divers",
         column(width = 4,h2("colonne 1", style="color:blue")),
         column(width =4,h3("colonne 2", style="color:green")),
         column(width =4,h4("Colonne 3",style="color:red")))
```


<details>
<summary markdown="span">**Voir le code**</summary>

```r
## app.R ##
library(shiny)
library(shinydashboard)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet2", text ="Table", badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    )
  ),
  
  dashboardBody(
    fluidRow(
      infoBox(title = "Date", value = Sys.Date(), icon = icon("credit-card")),
      infoBox(title = "IdepUser", value = toupper(Sys.getenv("USERNAME")), icon = icon("credit-card")),
      infoBoxOutput(outputId = "siteInternet", width = 4)
    ),
    tabItems(
    
      
      tabItem(tabName="onglet2",
              tabBox(width = 10, id = "tabBox1",selected = "divers",
                     
                tabPanel(title = "stat"
                ),
                
                tabPanel(title = "graph" 
                ),
                
                tabPanel(title = "carte"
                ),
                
tabPanel(title="divers",
         column(width = 4,h2("colonne 1", style="color:blue")),
         column(width =4,h3("colonne 2", style="color:green")),
         column(width =4,h4("Colonne 3",style="color:red")))
              )
      )
      
    )
  )
)

server <- function(input, output) {
  output$siteInternet <- renderInfoBox({
    infoBox(
      " ",value="shinydashboard : le site", icon = icon("thumbs-up"),
      color = "blue", fill = TRUE, href = "https://rstudio.github.io/shinydashboard/index.html"
    )
  })
}
shinyApp(ui, server)
```
</details>

---

On peut imbriquer les column() pour façonner l’interface selon la volonté du développeur et/ou du concepteur

![](www/img/48 column 3.PNG)


```r
tabPanel(title="divers",
             column(width=6, column(width=12,offset = 3, h3("colonne 1", style="color:black")),
                         column(6,h3("colonne 1A", style="color:blue")),
                         column(6,h3("colonne 1A", style="color:red"))
              ),
              column(3,h3("colonne 2", style="color:green")),
              column(3,h3("Colonne 3",style="color:red"))
               )      
)
```

<details>
<summary markdown="span">**Voir le code**</summary>

```r
ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet2", text ="Table", badgeLabel = "deux", badgeColor = "green",icon = icon("blind"))
    ) ),
  
  dashboardBody(
    fluidRow(
      infoBox(title = "Date", value = Sys.Date(), icon = icon("credit-card")),
      infoBox(title = "IdepUser", value = toupper(Sys.getenv("USERNAME")), icon = icon("credit-card")),
      infoBoxOutput(outputId = "siteInternet", width = 4)
    ),
    tabItems(
      tabItem(tabName="onglet2",
              tabBox(width = 10, id = "tabBox1",selected = "divers",
                     tabPanel(title = "stat"
                     ),
                     tabPanel(title = "graph" 
                     ),
                     tabPanel(title = "carte"
                     ),
                     tabPanel(title="divers",
                              column(width=6, column(width=12,offset = 3, h3("colonne 1", style="color:black")),
                                     column(6,h3("colonne 1A", style="color:blue")),
                                     column(6,h3("colonne 1A", style="color:red"))),
                              column(3,h3("colonne 2", style="color:green")),
                              column(3,h3("Colonne 3",style="color:red"))
                     )      
              ) )  )
    
  )
)


server <- function(input, output) {
  output$siteInternet <- renderInfoBox({
    infoBox(
      " ",value="shinydashboard : le site", icon = icon("thumbs-up"),
      color = "blue", fill = TRUE, href = "https://rstudio.github.io/shinydashboard/index.html"
    )
  })
}
shinyApp(ui, server)
```
</details>

### 4.42 Eléments statiques

| Actions                         | Fonctions                         |
|-------------------------------- | --------------------------------- |
| Incorporer un lien web          | a()                               |
| Passer une ligne                | br()                              |
| Créer un paragraphe             | p()                               |
| Mettre en italique              | em()                              |
| Mettre en gras                  | strong()                          |
| Utiliser un style (de 1 à 6)    | h1(), h2(), h3(), h4(), h5(), h6()| 
| Ajouter une ligne horizontale	  | hr()                              |
| Ajouter une image               | img()                             |


---

![](www/img/49 elements statiques.PNG)


```r
p(style = "font-family:Impact", "Accéder aux pages Web de", br(),
  a("@ R Shiny", href = "https://shiny.rstudio.com/"), br(),
  a("@ R Shiny dashboard", href = "https://rstudio.github.io/shinydashboard/"),
  hr()
)
```


<details>
<summary markdown="span">**Voir le code**</summary>

```r
ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil", badgeLabel = "Un", badgeColor = "red",icon = icon("bath")),
      br(),
      menuItem(tabName="onglet2", text ="Table", badgeLabel = "deux", badgeColor = "green",icon = icon("blind")),
      br(),tags$hr(),
      
      menuItem(actionButton("quit", label = "Quitter", width = "80%")),
      hr(),
      p(style = "font-family:Impact", "Accéder aux pages Web de", br(),
        a("@ R Shiny", href = "https://shiny.rstudio.com/"), br(),
        a("@ R Shiny dashboard", href = "https://rstudio.github.io/shinydashboard/"),
        hr()
      )
    )
  ),
  dashboardBody(
    tabItems(
      
      tabItem(tabName="onglet1",
              h1("Bienvenue sur la page d'accueil", style="color:blue")
      ),
      
      tabItem(tabName="onglet2",
              h1("Bienvenue sur la page de donnÃ©es tabulÃ©es", style="color:blue")
      )
      
    )
  )
)
server <- function(input, output) { }
shinyApp(ui, server)
```
</details>

## **Exercice 6-6**

**Réaliser l'interface suivante. Consignes :**

![](www/img/49 Exo 6_6.PNG)

**Réaliser cet écran. Consignes : **

<details>
<summary markdown="span">**Correction Exo 6-6**</summary>

```r
library(shinydashboard)
library(rio)

ui <- dashboardPage(
  dashboardHeader(title = "formation shiny", titleWidth = 300,
                  dropdownMenu(type = "messages",
                               messageItem(from = "message 1", message = "texte message 1"),
                               messageItem( from = "message 2", message = "texte message 2", icon = icon("question"),  time = "13:45"),
                               messageItem(from = "message 3", message = "texte message 3", icon = icon("life-ring"), time = "2014-12-01")
                  ),
                  dropdownMenu(type = "notifications", badgeStatus = "warning",
                               notificationItem(text = "2627 utilisateurs", icon("users")),
                               notificationItem(text = "187 articles", icon("truck"), status = "success"),
                               notificationItem(text = "Problèmes de connexion ce week-end", icon = icon("exclamation-triangle"), status = "warning")
                  ),
                  dropdownMenu(type = "tasks",
                               badgeStatus = "success",
                               taskItem(value = 5, color = "purple", "guide pédagogique"),
                               taskItem(value = 90, color = "green", "exercice de formation "),
                               taskItem(value = 60, color = "red", "exercice de synthèse"),
                               taskItem(value = 80, color = "yellow", "support de cours")
                  )
  ),
  dashboardSidebar(width = 300,
                   sidebarMenu(
                     menuItem(tabName="onglet1", text="Onglet 1"),
                     tags$hr(),
                     menuItem(tabName="onglet2", text ="Onglet 2", badgeLabel="Exercice", badgeColor="green", selected = TRUE),
                     tags$hr(),
                     menuItem(actionButton("quit", label = "Au revoir et à bientôt", width = "80%") ),
                     hr(),
                     p(style = "font-family:Impact", "Accéder aux pages Web de", br(),
                       a("@ R Shiny", href = "https://shiny.rstudio.com/"), br(),
                       a("@ R Shiny dashboard", href = "https://rstudio.github.io/shinydashboard/"),
                       hr()
                     )
                   )
  ),
  dashboardBody(
    fluidRow(
      infoBox(title = "Nous sommes le : ", value = Sys.Date(), width = 6, icon = icon("credit-card")),
      infoBoxOutput(outputId = "siteInternet", width = 6)
    ),
    tabItems(
      tabItem(tabName= "onglet1",
              h1("Bienvenue sur l'onglet 1", style="color:blue")
      ),
      tabItem(tabName= "onglet2",
              box(id="box0", status = "danger", solidHeader = TRUE ,
                  title = "Exercice de mise en page", width = 12, collapsible = TRUE, collapsed = FALSE,
                  h3("Box numéro 1", style="color : darkBlue"),
                  fluidRow(
                    column(3,
                           h4("1 - Libellés :", style="color : blue"),
                           h5("- libellé 1a ...", style="color : steelBlue")
                    ),
                    column(3,
                           h4("2 - Libellés :", style="color : blue"),
                           h5("- libellé 2a ...", style="color : steelBlue")
                    ),
                    column(3,
                           h4("3 - Libellés :", style="color : blue"),
                           h5("- libellé 3a ...", style="color : steelBlue")                    ),
                    column(3,
                           h4("4 - Libellés :", style="color : blue"),
                           h5("- libellé 4a ...", style="color : steelBlue"),
                           h5("- libellé 4b ...", style="color : steelBlue")
                    )
                  ),
                  column(width=6, offset = 3,         
                         box(id="boxInfo", status = "warning", solidHeader = FALSE, width = 12,
                             h4("5 - Libellés :", style="color : blue"),
                             h5("- libellé 5a ...", style="color : steelBlue"),
                             h5("- libellé 5b ...", style="color : steelBlue"),
                             h5("- libellé 5c ...", style="color : steelBlue"),
                             p("(ceci est un paragraphe inutile)")
                         )
                  )
              ),
              
              box(id="box3", status = "success", solidHeader = TRUE , width = 6, height = 410,
                  title = "Critères de filtre 1/4",  collapsible = TRUE, collapsed = FALSE,
                  
                  fluidRow(
                    column(width=7, offset = 0,
                           selectInput(inputId = "chxTer", label = "Choisir votre zonage",
                                       choices=c("Sélectionner un territoire","France entière",
                                                 "Régions"="REGION","Départements"="D"), multiple = F),
                           
                           selectInput(inputId = "reg", label = "Sélectionner  la (ou les) région(s) de votre choix",
                                       choices=c("Hauts de France","Aquitaine","Normandie","Ile de France"), multiple = T),
                           
                           selectInput(inputId = "dep", label = "Sélectionner  le (ou les) département(s) de votre choix",
                                       choices=c("Somme","Aisne","Oise","Nord","Pas-de-Calais","Haute-garonne","Essonne","Ardèche","Calvados"),
                                       multiple = T)
                    ),
                    column(width=5,
                           
                           radioButtons(inputId = "button1", label = "choisir une commune ",choices=c("Paris","Lille","Lyon","Marseille","Toulouse","Bordeaux","Cayeux-sur-mer")
                           )
                    )
                  ),
                  
                  box(id="box4", status = "success", solidHeader = TRUE ,
                      title = "Critères de filtre 2/4", width = 6, collapsible = TRUE, collapsed = TRUE,
                      h3("box 4 (fermée)")
                      
                  ),
                  box(id="box5", status = "success", solidHeader = TRUE ,
                      title = "Critères de filtre 3/4", width = 6, collapsible = TRUE, collapsed = TRUE,
                      h3("box 5 (fermée)")
                  ),
                  box(id="box6", status = "primary", solidHeader = TRUE ,
                      title = "Critères de filtre 4/4", width = 12, collapsible = TRUE, collapsed = TRUE,
                      h3("box 6 (fermée)")
                  )
              ),
              
              tabBox(selected = "Panneau 2", width = 6, id = "tabBox1", height = 410,
                     
                     tabPanel(title = "Panneau 1"),
                     
                     tabPanel(title = "Panneau 2", 
                              h4("Panneau 2", style="color: SteelBlue")
                     ),
                     
                     tabPanel(title = "Panneau 3"),
                     
                     tabPanel(title = "Panneau 4" )
              )
      )
    )
  )
)

server <- function(input, output) {
  output$siteInternet <- renderInfoBox({
    infoBox(
      "shinydashboard",value="Accéder au site web", icon = icon("thumbs-up"),
      color = "blue", fill = TRUE, href = "https://rstudio.github.io/shinydashboard/index.html"
    )
  })
  
  observeEvent(input$quit, stopApp())
}
shinyApp(ui, server)
```
</details>

# **5. Les fonctions du server**

* 5.1 La gestion des `input`
* 5.2 Opérer des traitements, créer des objets à partir d'une valeur réactive en amont des fonctions réactives `render*()`
* 5.3 Attendre la validation de l'utilisateur avant la mise à jour des sorties
* 5.4 Effectuer des actions
* 5.5 Transformer une valeur réactive en valeur non réactive

## 5.1 La gestion des `input`

Dans une application Shiny, il est souvent nécessaire de s'assurer que les valeurs des input (notamment) sont bien disponibles avant de réaliser des calculs ou d'afficher des graphiques.

La fonction req() permet de bloquer l'exécution d'une action tant que les valeurs requises ne sont pas disponibles.


```r
fileInput("chemin_tab","Selectionner le fichier RDS",
                           buttonLabel = "Charger..." )
```


```r
 output$tableau <- renderDT({
    readRDS(file =input$chemin_tab$datapath )
  })
```

![](www/img/51 req 1.PNG)

Il y a un message d’erreur, si le fichier n’est pas chargé

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(shinydashboard)
library(DT)

ui <- dashboardPage(
          dashboardHeader(title = "formation shiny"),
          dashboardSidebar(),
          dashboardBody(
            fileInput("chemin_tab","Selectionner un fichier RDS",
                      buttonLabel = "Charger..." ),
            DTOutput("tableau")
            
          )
  )


server <- function(input, output){ 
  output$tableau <- renderDT({
    readRDS(file =input$chemin_tab$datapath )
  })}
  
shinyApp(ui, server)
```
</details>

---

En utilisant le req(), il n’ y a plus de message d’erreur, si le fichier n’est pas chargé


```r
 output$tableau <- renderDT({
readRDS(file =req(input$chemin_tab$datapath) )
  })
```

![](www/img/51 req 2.PNG)

Une fois le fichier chargé

![](www/img/51 req 3.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(shinydashboard)
library(DT)

ui <- dashboardPage(
          dashboardHeader(title = "formation shiny"),
          dashboardSidebar(),
          dashboardBody(
            fileInput("chemin_tab","Selectionner un fichier RDS",
                      buttonLabel = "Charger..." ),
            DTOutput("tableau")
            
          )
  )


server <- function(input, output){ 
  output$tableau <- renderDT({
    readRDS(file =req(input$chemin_tab$datapath ))
  })}
  
shinyApp(ui, server)
```
</details>


## 5.2 Opérer des traitements, créer des objets à partir d'une valeur réactive en amont des fonctions réactives `render*()`

### 5.21 Un exemple pour y croire

On souhaite faire : 

* un tableau
* un graphique 
    - à partir d’un même tirage

Les deux Outputs sont construits à partir de deux tirages différents

**Dans le** `server`

```r
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = "skyblue", color = "black",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
```


```r
  output$resumeStat <- renderPrint({
    summary(rnorm(input$nbvaleurs))
  })
```

Dans chacune des fonctions `render` réalisant l'histogramme et le résumé statistique, un nouveau tirage aléatoire est réalisé.  
La modification d'une valeur réactive engendre le déclenchement de toutes les fonctions réactives dans lesquelles elle est impliquée.

**1er tirage**
![](www/img/52 exemple pour croire 1.PNG)

**2ème tirage**
![](www/img/52 exemple pour croire 2.PNG)

=> **On a deux tirages différents**

<details>
<summary markdown="span">**Voir le code**</summary>

```r
#Chargement des Packages
library(shiny)
library(ggformula)
toto<-2
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",value=50,min=1,max=500),
  textInput("titreGraphique",label="Entrer le titre du graphique",
            value="Histogramme de valeurs aléatoires suivant une loi normale"),
  #Liste déroulante pour le choix de l'année à représenter
  plotOutput("graphique"),
  verbatimTextOutput("resumeStat")
)
#serveur de calculs
server <- function(input, output) {
  

  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = "skyblue", color = "black",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
  output$resumeStat <- renderPrint({
    if (toto==2) summary(rnorm(input$nbvaleurs))
  })
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

**On fait un tirage différent pour chaque Output**


```r
  output$resumeStat1 <- renderPrint(
    #nombre()
    rnorm(input$nbvaleurs)
  )
```

![](www/img/52 exemple pour croire 3.PNG)


```r
  output$resumeStat2 <- renderPrint(
    #nombre()
    rnorm(input$nbvaleurs)
  )
```

![](www/img/52 exemple pour croire 4.PNG)
  
**Les deux distributions ne sont pas identiques** 

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(ggformula)

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
    label="Choisir un nombre de valeurs",
    value=10,min=1,max=500),
  textInput("titreGraphique",
    label="Entrer le titre du graphique",
    value="Histogramme de valeurs aléatoires suivant une loi normale"),
  
  verbatimTextOutput("resumeStat1"),
  verbatimTextOutput("resumeStat2")
)
#serveur de calculs
server <- function(input, output) {
  
  nombre <- reactive( rnorm(input$nbvaleurs) )
  
  output$resumeStat1 <- renderPrint(
    #nombre()
    rnorm(input$nbvaleurs)
  )

  output$resumeStat2 <- renderPrint(
    #nombre()
    rnorm(input$nbvaleurs)
  )
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

Comment construire les deux `Output` à partir des mêmes données ? 

* en utilisant une fonction `reactive()` pour construire et retourner un résultat : ici celui de la fonction `rnorm()`. 


```r
nombre <- reactive(rnorm(input$nbvaleurs) )
```
`nombre()` est l'expression réactive intermédiaire.  


```r
output$resumeStat1 <- renderPrint(
  nombre()
)
```


```r
output$resumeStat2 <- renderPrint(
  nombre()
)
```

On appelle cette fonction `nombre()` dans chaque `output` concerné.
A chaque modification de l’`input$nbvaleurs`, le tableau statistique et l’histogramme seront générés à partir des mêmes résultats et les deux distributions seront désormais identiques.

![](www/img/52 exemple pour croire 5.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(ggformula)

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
    label="Choisir un nombre de valeurs",
    value=10,min=1,max=500),
  textInput("titreGraphique",
    label="Entrer le titre du graphique",
    value="Histogramme de valeurs aléatoires suivant une loi normale"),
  
  verbatimTextOutput("resumeStat1"),
  verbatimTextOutput("resumeStat2")
)
#serveur de calculs
server <- function(input, output) {
  
  nombre <- reactive( rnorm(input$nbvaleurs) )
  
  output$resumeStat1 <- renderPrint(
    nombre()
  )

  output$resumeStat2 <- renderPrint(
    nombre()
  )
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

![](www/img/52 exemple pour croire 6.PNG)

### 5.22 Les fonctions réactives

On utilise des fonctions `reactive()` pour travailler sur les valeurs réactives en amont des fonctions de type `render*()`.
Elle permettent d’utiliser plusieurs fois un même résultat, pour lequel il y a qu’un seul traitement.
Ces fonctions `reactive()` seront relancées chaque fois que l'une des valeurs réactives qu'elles utilisent est mise à jour.
Ce sont des intermédiaires entre `input$` et `output$`.

---

* elles sont utilisées pour créer des objets à partir des valeurs réactives. Ils seront renvoyés vers les fonctions `render*()` ou vers d'autres fonctions `reactive()`.

* l'appel des fonctions `reactive()` se fait comme pour une fonction sans paramètre. Dans notre exemple : `nombre()`.

**Dans notre exemple, pourquoi cela règle le problème ?**
La fonction `reactive()` (`nombre()`) va permettre de ne lancer qu'une seule fois le tirage des valeurs aléatoires.
Les objets qu'elle retournera seront utilisées dans chacune des deux fonctions `render*()` : celles-ci se lanceront donc sur la même distribution.

![](www/img/53 Schema.PNG)

Une fonction  `reactive()` garde en mémoire la dernière valeur calculée à partir de ses `inputs`. 
On peut appeler plusieurs fois le résultat de la fonction, elle ne refait pas de calculs.

## **Exercice 7**
A partir de ("dashboard_Exo_7_original") modifier le programme pour que les données soient importées via un bouton.
**Développer les consignes**

![](www/img/53 Exo 7.PNG)

<details>
<summary markdown="span">**Correction Exo 7**</summary>

```r
library(shiny)
library(shinydashboard)
library(haven)
library(dplyr)
library(ggformula)
library(DT)

#pop <- readRDS("V:/DR13-SED/Fichiers Communs du Sed/_FormationR_shiny/base/poplegale_6815.RDS")
choix_annee <- c("Pop2015", "Pop2010", "Pop2006" ,"Pop1999", "Pop1990", "Pop1982", "Pop1975", "Pop1968")


ui <- dashboardPage(
  dashboardHeader(title = "formation shiny"),
  
  dashboardSidebar(
    sidebarMenu(
      menuItem(tabName="onglet1", text="Accueil",badgeLabel ="un" ,badgeColor ="green",
               icon = icon("bath") ),
      menuItem(tabName="onglet2", text ="Table",badgeLabel ="deux" ,badgeColor ="red",
               icon=icon("blind"))
    )
  ),
  

    dashboardBody(
      infoBox(title="Date",value=Sys.Date(),icon=icon("bell")),
      tabItems(
        tabItem( tabName="onglet1",
                 fileInput("chemin_tab","Selectionner un fichier RDS",
                           buttonLabel = "Parcourir..." ),
                 box(id="box1", status = "primary", solidHeader = T , background = "light-blue", 
                     title = "Choix du millesime",
                     collapsible = TRUE,collapsed = FALSE,
                     selectInput(inputId= "ANNEE", label="Choisir une annee",
                                 choices= choix_annee)
                 ),
                 box(id="box2", status = "success", solidHeader = TRUE , 
                     title = "Table de données", width = 12, collapsible = TRUE, collapsed = FALSE,
                    
                     DTOutput("tableau") )),
        tabItem (tabName="onglet2",
                 tabBox(width = 12,title="Bonjour",id="tabox1",
                        tabPanel(title="stat",
                                 box(id="box3",status = "success", width = 12,
                                     selectInput(inputId= "ANNEE2", label="Choisir une annee",
                                                 choices= choix_annee),
                                     DTOutput("tableau2"))
                        ),
                        tabPanel(title="graph"),
                        tabPanel(title="carte"),
                        tabPanel(title="divers",
                                 column(width=6,
                                        column(width=12,offset = 3, h4(strong("colonne 1"), style="color:black")),
                                        column(6,h4("colonne 1A", style="color:blue")),
                                        column(6,h4("colonne 1A", style="color:red"))
                                        ),
                                 column(3,h3("colonne 2", style="color:green")),
                                 column(3,h4("Colonne 3",style="color:red"))
                                 )
                        )
                 ) 
        )
  )
)

server <- function(input, output){
   pop2 <- reactive({
     readRDS(file =req(input$chemin_tab$datapath ))
   })
  output$tableau2 <- renderDT({ 
    pop2() %>% group_by(D) %>% 
      summarise_at(input$ANNEE2,sum,na.rm=T) 
  })
   output$tableau <- renderDT({ 
     pop2() %>% group_by(REGION) %>% 
       summarise_at(input$ANNEE,sum,na.rm=T) 
 })
  
}

shinyApp(ui, server)
```
</details>

## **Exercice 8**
- A partir de ("Exo_8_original") modifier le programme pour que le graphique ne change pas chaque fois qu’on change la couleur.

**Développer les consignes**

![](www/img/53 Exo 8.PNG)

<details>
<summary markdown="span">**Correction Exo 8**</summary>

```r
library(shiny)
library(shinydashboard)
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"
titre <-"EXO_8"
couleur <- "red"
couleur_bord <- "black"
valmin <- 5

ui <- dashboardPage(
  dashboardHeader(title = titre),
  dashboardSidebar(disable=T),
  dashboardBody(      column(6,
        verticalLayout(
          sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",
                      value=50,min=valmin,max=500),
          textInput("titreGraphique",label="Entrer le titre du graphique",
                  value=titre_graphique)
        )
      ),
      column(6,
        verticalLayout(
          selectInput("border_hist","Couleur des bordures",
                    choices = c("noir"="black","bleu"="blue","rouge"="red",
                                "orange"="orange","violet"="purple","vert"="green"),
                    selected = couleur_bord ),
          selectInput("coul_hist","Couleur du graphique",
                    choices = c("bleu"="blue","rouge"="red","orange"="orange",
                                "violet"="purple","vert"="green"),
                    selected = couleur)
             )
      ),
      plotOutput("graphique")
    ))

server <- function(input, output) {
  valeur<- reactive(
    rnorm(input$nbvaleurs)
  )
  
  output$graphique <- renderPlot({
    gf_histogram(~ valeur(),fill = input$coul_hist,
                 color = input$border_hist,
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
}

shinyApp(ui, server)
```
</details>

## 5.3 Attendre la validation de l'utilisateur avant la mise à jour des sorties

* 5.31 Les fonctions `eventreactive()`
* 5.32

### 5.31 Les fonctions `eventreactive()`

**Contexte :**
On souhaite que la mise à jour des sorties d'une application soit mise en attente jusqu'à la validation de l'utilisateur, par clic sur un bouton

Avant validation
![](www/img/54 eventReactive 1.PNG)

Après validation
![](www/img/54 eventReactive 1.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(ggformula)

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
              label="Choisir un nombre de valeurs",
              value=50, min=1,max=500),
  
  actionButton("validbouton","Validez votre choix"),
  
  plotOutput("graphique")
  
)
#serveur de calculs
server <- function(input, output) {
  nombre <- eventReactive(input$validbouton , rnorm(input$nbvaleurs) )
  output$graphique <- renderPlot(
    gf_histogram(~ nombre(),fill = "skyblue", color = "black",
                 title="Graphique") %>% 
    gf_theme(plot.title = element_text(hjust = 0.5))
  )
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

**Fonctionnement**

* 1- On crée le bouton de validation avec la fonction actionButton()


```r
actionButton("validbouton","Validez votre choix")
```

* 2- On crée une expression réactive dont la réactivité est conditionnée à la validation du bouton d'action, avec la fonction eventReactive()


```r
nombre <- eventReactive(input$validbouton , rnorm(input$nbvaleurs) )
```

* 3- le résultat du eventReactive est utilisé dans la fonction renderPlot


```r
  output$graphique <- renderPlot(
    gf_histogram(~ nombre(),fill = "skyblue", color = "black",
                 title="Graphique") %>% 
    gf_theme(plot.title = element_text(hjust = 0.5))
  )
```

---

La fonction `eventReactive()` permet de créer une expression réactive qui ne s'exécute que lorsque le bouton a été validé

![](www/img/54 eventReactive 3.PNG)

---

![](www/img/54 eventReactive 4.PNG)


## **Exercice 9**
A partir de ("dashboard_Exo_9_original") modifier le programme pour que l’utilisateur puisse choisir d’afficher deux types de graphiques différents en important la table **Projection.xls**. Prévoir un bouton de validation pour que le graphique se mette à jour.
**Développer les consignes**

![](www/img/54 eventReactive Exo9.PNG)

<details>
<summary markdown="span">**Correction Exo 9**</summary>

```r
library(shiny)
library(shinydashboard)
library(readxl)
library(dplyr)
library(shinyFiles)
library(DT)
library(rio)
library(ggformula)

#memory.limit(size=64048576)

dep <- c("75","77","78","91","92","93","94","95")

ui <- dashboardPage(
  dashboardHeader(title = "Projection"),
  dashboardSidebar(
    sidebarMenu(
      menuItem(text = "onglet 1",tabName = "onglet1")
    )
  ),
  dashboardBody(
    tabItems(
      tabItem(tabName = "onglet1",
      column(6,
        verticalLayout(
          box(title="Paramétrage",status="success", solidHeader = T, background = "purple",
            shinyFilesButton(id = "fichier", label = "Charger une table", 
                             title = "Veuillez sélectionner la table svp", multiple = FALSE),
            width = 12),
          box(textInput(inputId = "titregraph",label = "Choisir un titre"),width = 12)
             )
      ),
      column(6,
             box(selectInput(inputId = "D",label = "Choisir un departement",
                             choices = dep
             ),width = 12),
             column(6,
             radioButtons("choix_g", "Choisir un graphique",selected = "g1",
                          choices = c("Nuage de points"="g2","Courbe"="g1"))),
             column(6,actionButton("B1","Valider"))
      ),
      box(plotOutput("graph"),width = 12)
      )
    )
  )
)

server <- function(input, output) {
  r_tab <- reactive({
    shinyFileChoose(input, "fichier", roots = c(v = "V:/", Z = "Z:/"))
    import(as.character(parseFilePaths(c(v = "V:/", Z = "Z:/"), req(input$fichier))$datapath))
  })
  g1 <- reactive({
    r_tab() %>% mutate(ANNEE=as.numeric(ANNEE),POP=as.numeric(POP)) %>%  
      filter(DEP==req(input$D)) %>% 
      gf_line(POP ~ ANNEE) 
  })
    
  g2 <- reactive({
    r_tab() %>% mutate(ANNEE=as.numeric(ANNEE),POP=as.numeric(POP)) %>%  
      filter(DEP==req(input$D)) %>% 
      gf_point(POP ~ ANNEE) 
  })
  g <- eventReactive(list(input$B1,input$fichier),{
    if (input$choix_g=="g1") g1() else g2()
  })
  output$graph<-renderPlot({
    g()%>% 
      gf_labs(title=input$titregraph) %>% 
      gf_theme(plot.title=element_text(hjust=0.5))
  })
}

shinyApp(ui, server)
```
</details>

### Focus `shinyFileButton / shinyFileChoose()`

![](www/img/55 shinyfilechoose.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>


```r
library(shiny)
library(ggformula)
library(shinyFiles)
library(DT)
library(haven)
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  shinyFilesButton(id = "fichier", 
                   label = "Charger une table", 
                   title = "Veuillez sélectionner la table svp", 
                   multiple = FALSE),
  DTOutput("tableau")
)
#serveur de calculs
server <- function(input, output) {
  r_tab <- reactive({
    shinyFileChoose(input, "fichier", roots = c(v = "V:/", Z = "Z:/"))
    tabinfo <- parseFilePaths(c(v = "V:/", Z = "Z:/"), req(input$fichier))
    read_sas(as.character(tabinfo$datapath), encoding = "UTF-8")
  })
  output$tableau <- renderDT(r_tab())
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>


## 5.4 Effectuer des actions

### 5.41 Les fonctions observers
On va expliciter le rôle de différents types d'`obsevers` : les fonctions `observe()` et `observeEvent`.

Utilité des `observers` dans des applications réactives :

* Pour le gestionnaire de l'application (log, sauvegarde...)
* Réaliser des traitements sans que ceux-ci n'ait de conséquences sur les éléments utilisés et affichés pour l'utilisateur
* Modifier l’interface

---

Les `observers` permettent de déclencher une action lorsqu’un champ est modifié.

Il existe 2 cas :

* `observe()`: est appelé à chaque modification de la valeur d’un input cité dans la fonction.

![](www/img/56 observe.PNG)

<details>
<summary markdown="span">**Copier ce code et le tester dans RStudio**</summary>
Observer le résultat dans votre console


```r
library(shiny)
library(ggformula)
library(dplyr)

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
              label="Choisir un nombre de valeurs",
              value=50, min=1,max=500)
  
)
#serveur de calculs
server <- function(input, output) {
  observe( print(input$nbvaleurs) )
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

* `observeEvent()`: est appelé à chaque modification de la valeur d’un `input` cité dans le 1er argument de la fonction.

![](www/img/56 observeEvent.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(ggformula)

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
              label="Choisir un nombre de valeurs",
              value=50, min=1,max=500),
  
  actionButton("validbouton","Validez votre choix")
  
)
#serveur de calculs
server <- function(input, output) {
  observeEvent( 
    input$validbouton, print(input$nbvaleurs)
  )
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

* **observe()**


```r
observe(print(input$nbValeurs))
```
le `print()` s’exécute à chaque modification de la valeur de `input$nbValeurs`. Il est donc exécuté à la modification d'un `input`.

* **observeEvent()**


```r
observeEvent(input$validBouton, print(input$nbValeurs))
```
le `print()` s’exécute à chaque « clic » sur le bouton `input$validBouton`.

Il est donc exécuté à la modification d'un `input` **ET** à la confirmation par un `input` de validation.

---

### 5.42 Exemples d’utilisation des observers


**Fermer l'application avec un bouton*


```r
library(shiny)
library(ggformula)
library(dplyr)

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
              label="Choisir un nombre de valeurs",
              value=50, min=1,max=500),
  actionButton("stop","Stop")
  
)
#serveur de calculs
server <- function(input, output) {
  observe( print(input$nbvaleurs) )
  observeEvent(input$stop,stopApp())
}

# Run the application 
shinyApp(ui = ui, server = server)
```

La fonction `observeEvent` déclenche l'action lors d'un clic sur le bouton.

---

**Exemple d'utilisation de l'`observeEvent`**

![](www/img/56 observeEvent graph.PNG)

Le bouton *Validez votre choix* permet de générer le graphique : l’`output` *graphique* est créé quand on clique sur le bouton.  
Toutefois, le graphique est modifié chaque fois qu’on choisit une nouvelle valeur, sans validation.

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(ggformula)

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
              label="Choisir un nombre de valeurs",
              value=50, min=1,max=500),
  
  actionButton("validbouton","Validez votre choix"),
  plotOutput("graphique")
  
)
#serveur de calculs
server <- function(input, output) {
  
  nombre <- reactive(rnorm(input$nbvaleurs))
  
  observeEvent(input$validbouton,
               output$graphique <- renderPlot(
                 gf_histogram(~ rnorm(input$nbvaleurs),fill = "skyblue", color = "black",
                              title="Graphique") %>% 
                   gf_theme(plot.title = element_text(hjust = 0.5))
               ))
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

**sauvegarder une table, sans choisir le chemin** : `ggsave`

![](www/img/56 observeEvent ggsave.PNG)

On récupère un dataframe qui contient le chemin sélectionné par l’utilisateur. On l’utilise ici pour 
paramétrer la fonction `ggsave`.
La table est sauvegardée au format `.RDS` dans le dossier `donnees` de ce projet.

<details>
<summary markdown="span">**Copier ce code et le tester dans RStudio**</summary>

```r
library(shiny)
library(haven)
library(dplyr)
library(rio)
library(glue)
# Chargement et traitement sur la table des populations de 68 à 2015  dans l'environnement global
pop<- read_sas("donnees/poplegale_6815.sas7bdat",encoding = "utf8") %>% 
  #changement des noms des colonnes de population
  rename_if(is.numeric,~(ifelse(substr(.,5,6)<16,
                                paste0("Pop_20",substr(.,5,6)),
                                paste0("Pop_19",substr(.,5,6)))))

#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  textInput("nomFic", "nom du fichier"),
  actionButton("validbouton","Validez votre choix")
)
#serveur de calculs
server <- function(input, output) {
  observeEvent(input$validbouton,{
    progress <- Progress$new( min=0, max=3)#progress
    progress$set( value = 1, message="Sauvegarde des données en cours")#progress
    export(pop,glue("donnees/{input$nomFic}.rds"))
    progress$set( value = 3, message="Sauvegarde des données en cours")
    progress$close()
  })
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

**shinyFileSave**

![](www/img/56 observeEvent shinyfilesave.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(ggformula)
library(shinyFiles)
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  sliderInput(inputId = "nbvaleurs",
              label="Choisir un nombre de valeurs",
              value=50, min=1,max=500),
  
  actionButton("validbouton","Validez votre choix"),
  shinySaveButton("b_sauve", "Sauvegarder", 'Sauvegarder sous ...',filetype=list(pdf="pdf")),
  plotOutput("graphique")
  
)
#serveur de calculs
server <- function(input, output) {
  observe({
    shinyFileSave(input, "b_sauve", roots=c(c = "C:/", Z = "Z:/"))
    che <- parseSavePath(roots=c(c = "C:/",Z = "Z:/"), req(input$b_sauve))
    ggsave(as.character(che$datapath))
  })
  
  nombre <- eventReactive( input$validbouton , rnorm(input$nbvaleurs) )
  output$graphique <- renderPlot(
    gf_histogram(~ nombre(),fill = "skyblue", color = "black",
                 title="Graphique") %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  )
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

## **Exercice 10**
**Développer les consignes**

![](www/img/56 observeEvent Exo 10.PNG)

<details>
<summary markdown="span">**Correction Exo 10 ( A revoir un peu) **</summary>

```r
library(shiny)
library(shinydashboard)
library(readxl)
library(dplyr)
library(shinyFiles)
library(DT)
library(rio)
library(ggformula)

chemin_save <- "donnees"

dep <- c("75","77","78","91","92","93","94","95")

ui <- dashboardPage(
  dashboardHeader(title = "Projection"),
  dashboardSidebar(
    sidebarMenu(
      menuItem(text = "onglet 1",tabName = "onglet1")
    )
  ),
  dashboardBody(
    tabItems(
      tabItem(tabName = "onglet1",
      column(6,
        verticalLayout(
          box(title="Paramétrage",status="success", solidHeader = T, background = "purple",
            shinyFilesButton(id = "fichier", label = "Charger une table", 
                             title = "Veuillez sélectionner la table svp", multiple = FALSE),width = 12),
          box(textInput(inputId = "titregraph",label = "Choisir un titre"),
              actionButton("b_titre","Valider"),
              width = 12)
             )
      ),
      column(6,
             box(selectInput(inputId = "D",label = "Choisir un departement",
                             choices = dep
             ),width = 12),
             column(6,
             radioButtons("choix_g", "Choisir un graphique",selected = "g1",
                          choices = c("Nuage de points"="g2","Courbe"="g1"))),
             column(6,actionButton("B1","Valider"))
      ),
      actionButton("b_save","Sauvegarder la base "),
      shinySaveButton("b_save2", "Sauvegarder le graphique", 'Sauvegarder sous ...', filetype=list(pdf="pdf")),
      box(plotOutput("graph"),width = 12)
      )
    )
  )
)

server <- function(input, output) {
  
  r_tab <- reactive({
    shinyFileChoose(input, "fichier", roots = c(c = "C:/", d = "D:/"))
    import(as.character(parseFilePaths(c(c = "C:/", d = "D:/"), req(input$fichier))$datapath))
  })
  
  r_titre <- eventReactive( input$b_titre, { input$titregraph },ignoreNULL = F)
  
  g1 <- reactive({
    r_tab() %>% mutate(ANNEE=as.numeric(ANNEE),POP=as.numeric(POP)) %>%  
      filter(DEP==req(input$D)) %>% 
      gf_line(POP ~ ANNEE) 
  })
    
  g2 <- reactive({
    r_tab() %>% mutate(ANNEE=as.numeric(ANNEE),POP=as.numeric(POP)) %>%  
      filter(DEP==req(input$D)) %>% 
      gf_point(POP ~ ANNEE)
  })
  
  g <- eventReactive(list(input$B1,input$fichier),{
    if (input$choix_g=="g1") g1() else g2()
  })
  
  observeEvent(input$b_save,{
    progress <- Progress$new( min=0, max=3)#progress
    progress$set( value = 1, message="Sauvegarde des données en cours")#progress
    saveRDS(r_tab(),paste0(chemin_save,"/save.RDS"))
    progress$set( value = 3, message="Sauvegarde des données en cours")
    progress$close()
  })
  
  observe({
    shinyFileSave(input, "b_save2", roots=c(c = "C:/", d = "D:/"))
    che <- parseSavePath(roots=c(c = "C:/", d = "D:/"), req(input$b_save2))
    progress <- Progress$new( min=0, max=3)#progress
    progress$set( value = 1, message="Sauvegarde des données en cours")#progress
    ggsave(as.character(che$datapath))
    progress$set( value = 3, message="Sauvegarde des données en cours")
    progress$close()
  })
  
  
  
  output$graph<-renderPlot({
    g() %>% 
      gf_labs(title=r_titre()) %>% 
      gf_theme(plot.title=element_text(hjust=0.5))
  })
  
}

shinyApp(ui, server)
```
</details>

## 5.5 Transformer une valeur réactive en valeur non réactive

### 5.51 Un exemple pour y croire


```r
#Chargement des Packages
library(shiny)
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"
titre <-"Isolate"
couleur <- "red"
valmin <- 5
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  
  # Titre de l'application
  titlePanel(titre),
  
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",value=50,min=valmin,max=500),
  textInput("titreGraphique",label="Entrer le titre du graphique",
            value=titre_graphique),
  
  plotOutput("graphique")
)
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = couleur, color = "black",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
  
}

# Run the application 
shinyApp(ui = ui, server = server)
```

La fonction `renderPlot()` fait appel à deux valeurs réactives :

* input$nbValeurs
* input$titreGraphique

Une fonction dynamique se relance entièrement chaque fois qu'une des valeurs réactives qu'elle utilise est modifiée

![](www/img/57 isolate 1.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>

```r
#Chargement des Packages
library(shiny)
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"
titre <-"Isolate"
couleur <- "red"
valmin <- 5
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  
  # Titre de l'application
  titlePanel(titre),
  
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",value=50,min=valmin,max=500),
  textInput("titreGraphique",label="Entrer le titre du graphique",
            value=titre_graphique),
  
  plotOutput("graphique")
)
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = couleur, color = "black",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
  
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

Comment faire en sorte que la mise à jour du titre du graphique ne vienne pas relancer l'ensemble de la fonction réactive ?

### 5.52 La fonction isolate

En utilisant la fonction `isolate()`. Elle permet de transformer une valeur réactive en valeur non réactive.

**Syntaxe :**

```r
isolate(input$titreGraphique)
```

Ainsi, dans l’exemple, en modifiant le titre, celui-ci ne se mettra à jour qu'à l'occasion d'un rafraîchissement de la page Web ou bien lors de la prochaine modification de l'autre valeur réactive (qui relancera la fonction `renderPlot()`)

![](www/img/57 isolate 2.PNG)

<details>
<summary markdown="span">**Voir le code**</summary>

```r
#Chargement des Packages
library(shiny)
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"
titre <-"Isolate"
couleur <- "red"
valmin <- 5
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  
  # Titre de l'application
  titlePanel(titre),
  
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",value=50,min=valmin,max=500),
  textInput("titreGraphique",label="Entrer le titre du graphique",
            value=titre_graphique),
  
  plotOutput("graphique")
)
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  output$graphique <- renderPlot({
    gf_histogram(~ rnorm(input$nbvaleurs),fill = couleur, color = "black",
                 title=isolate(input$titreGraphique)) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
  
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>

---

**L'`isolate()`, c'est pas automatique !**

**A EXPLIQUER !?!**

<details>
<summary markdown="span">**Voir le code**</summary>

```r
#Chargement des Packages
library(shiny)
library(ggformula)

titre_graphique <- "Histogramme de valeurs aléatoires suivant une loi normale"
titre <-"Isolate c'est pas automatique"
couleur <- "red"
valmin <- 5
#Début de l'application Shiny
#Interface
ui <- fluidPage( 
  # Titre de l'application
  titlePanel(titre),
  sliderInput(inputId = "nbvaleurs",label="Choisir un nombre de valeurs",value=50,min=valmin,max=500),
  textInput("titreGraphique",label="Entrer le titre du graphique",
            value=titre_graphique),
  plotOutput("graphique")
)
#serveur de calculs
server <- function(input, output) {
  #Création de l'output réactif aux modifications de la liste déroulante
  nombre <- reactive( rnorm(input$nbvaleurs))
  output$graphique <- renderPlot({
    gf_histogram(~ nombre(),fill = couleur, color = "black",
                 title=input$titreGraphique) %>% 
      gf_theme(plot.title = element_text(hjust = 0.5))
  })
}

# Run the application 
shinyApp(ui = ui, server = server)
```
</details>


---

Ce qu'un concepteur d'application pourrait vouloir faire :

* utiliser une valeur réactive à plusieurs reprises dans la partie `server` 
* faire des traitements avec la valeur réactive avant de l'envoyer dans une fonction `render*` 
    - => `reactive()`
* attendre la validation de l'utilisateur avant la mise à jour des sorties 
    - => `observEvent()` et `eventReactive()`
* réaliser des traitements qui ont lieu dans la partie `server` sans conséquence sur l'`ui`
    - => `observe()`
* transformer une valeur réactive en valeur non réactive
    - =>`isolate()`

## * 6. shinyfiles et interfaces dynamiques

Plusieurs approches d'interface dynamique :

* renderUI+uiOutput, qui permettent de générer dynamiquement des appels à des fonctions `Input`
* afficher/masquer des widgets (`conditionalPanel()`)

### 6.1 `renderUI` et `uiOutput`

Il est parfois nécessaire de générer dynamiquement l'interface et le contenu des widgets en réponse aux choix de l'utilisateur.

La fonction `renderUI()` permet de modifier l’interface depuis le `server`.
Dans l’`UI`, on utilise la fonction `uiOutput()`.

---

**Exemple 1 :**

l'utilisateur veut sélectionner un département. Il choisit d'abord la région concernée,
puis l'interface est mise à jour automatiquement pour lui proposer seulement les départements de cette région.

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shiny)
library(shinydashboard)
library(dplyr)

pop <- readRDS("donnees/poplegale_6815.RDS")
geographie <- pop %>% filter(REGION!="") %>% distinct(REGION,D)

ui <- dashboardPage(
  dashboardHeader(title = "Formation shiny"),
  dashboardSidebar(disable = T),
  dashboardBody(
    selectInput("region", "Choix de la région",c("",unique(geographie$REGION))), 
    uiOutput("choix_dep")
  )
)

server <- function(input, output){
  output$choix_dep <- renderUI(
    selectInput(inputId = "departement",
                label = "Choix du département",
                choices = unique(geographie$D[geographie$REGION == input$region])
    )
  )
}

shinyApp(ui, server)
```
</details>
---

**Exemple 2 :**

* créer une liste déroulante avec la liste des départements ou des régions du fichier chargé,
* afficher une 2ème liste déroulante qui ne s’affiche que lorsque les départements ou les régions sont sélectionnés avec les variables numériques du fichier.

<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shinydashboard)
library(dplyr)

pop <- readRDS("donnees/poplegale_6815.RDS")
geographie <- pop %>% filter(REGION!="") %>% distinct(REGION,D)

ui <- dashboardPage(
  dashboardHeader(title = "Formation shiny"),
  dashboardSidebar(disable = T),
  dashboardBody(
    radioButtons("ChoixZonage1", "Zonage", 
                 choices = c("Département" = "D", "Région" = "REGION")),
    uiOutput("ter1"),
    uiOutput("var1")
  ))

server <- function(input, output){
  r_ter<- reactive( unique(pop[, input$ChoixZonage1]) )
  r_var<- reactive( names(pop %>% select_if(is.numeric)) )
  output$ter1 <- renderUI( selectInput("W_ter", "Choix", r_ter(), multiple = TRUE) )
  output$var1 <- renderUI(
    if(!is.null(input$W_ter)) selectInput("W_var", "Choix", r_var(), multiple= TRUE)
  )
}

shinyApp(ui, server)
```
</details>
  
Exemple 2 bis :


<details>
<summary markdown="span">**Voir le code**</summary>

```r
library(shinydashboard)
library(dplyr)

pop <- readRDS("donnees/poplegale_6815.RDS")
geographie <- pop %>% filter(REGION!="") %>% distinct(REGION,D)

ui <- dashboardPage(
  dashboardHeader(title = "Formation shiny"),
  dashboardSidebar(disable = T),
  dashboardBody(
    radioButtons("ChoixZonage1", "Zonage", 
                 choices = c("Département" = "D", "Région" = "REGION")),
    uiOutput("ter1"),
    uiOutput("var1"),
    DTOutput("tableau")
  ))

server <- function(input, output){
  r_ter<- reactive( unique(pop[, input$ChoixZonage1]) )
  r_var<- reactive( names(pop %>% select_if(is.numeric)) )
  output$ter1 <- renderUI( selectInput("W_ter", "Choix", r_ter(), multiple = TRUE) )
  output$var1 <- renderUI(
    if(!is.null(input$W_ter)){
      sel_var <- input$W_var
      selectInput("W_var", "Choix", r_var(),selected=sel_var, multiple= TRUE)
    } 
  )
  tab_pop <- reactive({
    pop %>% filter(get(input$ChoixZonage1) %in% input$W_ter) %>% 
      group_by("Territoire"=get(input$ChoixZonage1)) %>%
      summarise_at(req(input$W_var),sum,na.rm=T)
  })
  output$tableau <- renderDT(
    tab_pop()
  )
}

shinyApp(ui, server)
```
</details>

---

**Ui :**

```r
radioButtons("export.type", "",
             c("Exporter la table courante" = 1,
               "Exporter une liste de tables (Excel xlsx)" = 2,
              "Sauvegarder une liste de tables (Rdata)" = 3))
```


```r
uiOutput("export.control")
```

**Server :**

```r
output$export.control <- renderUI(
    if (input$export.type>1)
     tagList(
       selectizeInput("export.tables",
                     if (input$export.type == 2) "Tables à exporter" else "Tables à sauvegarder", multiple = TRUE, options = list(placeholder = '<toutes>'), choices= listeTables),
       					         if (input$export.type==2) checkboxInput("export.names", "Préciser le nom des feuilles", FALSE) 
))
```

La fonction `renderUI()` reçoit en argument de 0 à n éléments de l’interface :

* 0 :	il ne se passe rien,
* 1 : un widget défini avec la syntaxe habituelle de l’UI,
* Sup 1 : plusieurs widgets reliés dans un tagList, box, fluidRow() ...

---

## Exercice 11

A partir de **"dashboard_Exo_11_original"*)**
Dans un l’onglet 2 créer les widget suivants :

* Choix de la ou les région(s),
* Choix des départements correspondant aux régions sélectionnées.
* Choix de la population à afficher
    - puis afficher la population par département qui correspond à la sélection

![](www/img/57 isolate Exo 11.PNG)

## Exercice 11 bis

Même exercice, en faisant en sorte que les widget s’affichent successivement

<details>
<summary markdown="span">**Correction Exo 11**</summary>


```r
library(shiny)
library(DT)
library(haven)
library(shinydashboard)
library(shinyFiles)
library(dplyr)
library(billboarder)
library(rio)
library(glue)
#Les variables
titre_appli<- "POP_APPLI"
# Les fonctions

header <- dashboardHeader(title=titre_appli)
sidebar <-dashboardSidebar(
  sidebarMenu(
    menuItem("Onglet 1",tabName ="O1",icon=icon("flask"),badgeColor = "blue")
    ,menuItem("Onglet 2",tabName ="O2",icon=icon("bullhorn"))
  ) 
)

body <- dashboardBody(
  tabItems(
    tabItem(tabName = "O1",
            box(title="Paramétrage",status="success", solidHeader = T, background = "purple",
                shinyFilesButton(id = "fichier",label = "Charger une table", 
                                 title = "Veuillez sélectionner la table svp", 
                                 multiple = FALSE),
                width = 12),
            DTOutput("tableau"))
    ,tabItem(tabName = "O2",
             box(title="Paramétrage",status="success", solidHeader = T, background = "purple",
                 column(4,uiOutput("ter1")),
                 column(4,uiOutput("ter2")),
                 column(4,uiOutput("var1")),
                 width = 12) ,
             box(DTOutput("tableau2") ,width = 12)
    )))

shinyApp(
  ui = dashboardPage(header, sidebar, body),
  server = function(input, output) {
    
    r_ter <- reactive(unique(r_tab()[,"REGION"]))
    
    r_ter2 <- reactive(unique(r_tab()[r_tab()[,"REGION"] %in% input$w_ter,"D"]))
    
    r_var <- reactive( names( r_tab() %>% select_if (is.numeric()) ) )
    
    output$ter1 <- renderUI({
      selectInput("w_ter","Choix des regions",r_ter(),multiple = T)
    })
    
    output$ter2 <- renderUI({
      selectInput("w_ter2","Choix des departements",r_ter2(),multiple = T)
    })
    
    output$var1 <- renderUI(
      selectInput("W_var","Choix population",r_var())
    )
    
    r_tab2 <- reactive({
      r_tab() %>% filter(D %in% input$w_ter2) %>% group_by(D) %>% 
        summarise_at(req(input$W_var),~sum(.,na.rm=T))
    })
    
    output$tableau2 <- renderDT(r_tab2())
    
    
    r_tab <- reactive({
      shinyFileChoose(input, "fichier", roots = c(v = "V:/", Z = "Z:/"))
      import(as.character(parseFilePaths(c(v = "V:/", Z = "Z:/"), req(input$fichier))$datapath))
    })
    
    r_var<- reactive({
      names(r_tab() %>% select_if(is.numeric))
    })
    
    output$tableau <- renderDT(r_tab())

  })
```
</details>


<details>
<summary markdown="span">**Correction Exo 11 - Bonus **</summary>

```r
library(shiny)
library(DT)
library(haven)
library(shinydashboard)
library(shinyFiles)
library(dplyr)
library(billboarder)
library(rio)
library(glue)
#Les variables
titre_appli<- "POP_APPLI"
# Les fonctions
f_datatable<-function(tab){
  datatable(tab, 
            extensions = c('Buttons','ColReorder','FixedColumns')
            ,options = list(
              dom = 'Blfrtip',
              buttons = c("colvis",'csv', 'excel', 'pdf'),
              colReorder = TRUE,
              lengthMenu = list(c(5, 15, -1), c('5', '15', 'Ensemble')),
              pageLength = 10
            )
  ) 
} 

header <- dashboardHeader(title=titre_appli)
sidebar <-dashboardSidebar(
  sidebarMenu(
    menuItem("Onglet 1",tabName ="O1",icon=icon("flask"),badgeColor = "blue")
    ,menuItem("Onglet 2",tabName ="O2",icon=icon("bullhorn"))
  ) 
)

body <- dashboardBody(
  tabItems(
    tabItem(tabName = "O1",
            box(title="Paramétrage",status="success", solidHeader = T, background = "purple",
                shinyFilesButton(id = "fichier",label = "Charger une table", 
                                 title = "Veuillez sélectionner la table svp", 
                                 multiple = FALSE),
                width = 12),
            DTOutput("tableau"))
    
    ,tabItem(tabName = "O2",
             box(title="Paramétrage",status="success", solidHeader = T, background = "purple",
                 column(4,uiOutput("ter1")),
                 column(4,uiOutput("ter3")),
                 column(4,uiOutput("var1")),
                 width = 12),
             box(
               DTOutput("tableau2") ,
               width = 12)
    )))

shinyApp(
  ui = dashboardPage(header, sidebar, body),
  server = function(input, output) {
    
    r_tab <- reactive({
      shinyFileChoose(input, "fichier", roots = c(v = "C:/", Z = "D:/"))
      import(as.character(parseFilePaths(c(v = "C:/", Z = "D:/"), req(input$fichier))$datapath))
    })
    
    output$tableau <- renderDT(r_tab())
    
    r_ter<- reactive(unique(r_tab()[,"REGION"]))
    
    output$ter1 <- renderUI(selectInput("W_ter","Choix région",r_ter(),multiple = T))
    
    observeEvent(input$W_ter,{
      l<-unique(r_tab() %>% filter(REGION%in%input$W_ter) %>% select(D) %>% pull)
      s <- reactive( input$W_ter2)
      output$ter3 <- renderUI(
        selectInput("W_ter2","Choix département",l,selected=s(),multiple = T))
    })

        observeEvent(input$W_ter2,{
      l2 <- names(r_tab() %>% select_if(is.numeric))
      s2 <- reactive(input$W_var)
      output$var1 <- renderUI(
          selectInput("W_var","Choix population",l2,selected=s2(),multiple = T)
      )
    })
    
    
    t_tab1 <- reactive({
      r_tab() %>% select("REGION","D",req(input$W_var) ) %>%
        filter(D%in%req(input$W_ter2)) %>% group_by(D) %>%
        summarise_if(is.numeric,sum,na.rm=T)
      
    })
    output$tableau2 <- renderDT(f_datatable(t_tab1()))
    
  })
```
</details>


### 6.2 `conditionalPanel`

Le `conditionalPanel` permet d'afficher un élément de l'interface (`widget` ou `output`) en fonction d'une (ou plusieurs) condition(s).

**Exemple (partiel) :**

```r
radioButtons(inputId = "graph",label = "voulez vous paramétrer le graphique ?",
             choices = c("oui","non"),selected = "non")
```
La condition est entre **" "** et contient un **.** au lieu d'un **$**.

```r
conditionalPanel(
  condition = "input.graph=='oui'",
  (sliderInput("bins",
                "Number of bins:",
                min = 1,
                max = 50,
                value = 30)
  ))
```

![](www/img/58 ConditionalPanel.PNG)

**Code à insérer ici pour l'exemple**

# 7. Bon à savoir

## 7.1 Afficher un visuel durant une opération longue

À insérer comme un widget


```html
div(id = "form",
    tags$script(
      'function checkifrunning() {
          var is_running = $("html").attr("class").includes("shiny-busy");
          if (is_running){
          $("#loading").show()
          } else {
          $("#loading").hide()
          }
          }; 
          setInterval(checkifrunning, 1000)'
    ), 
    tags$style(
      "#loading {
          display: inline-block;
          border: 3px solid #f3f3f3; 
          border-top: 3px solid #3498db; 
          border-radius: 50%;
          width: 50px;
          height: 50px;
          animation: spin 1s ease-in-out infinite;
          }
          
          @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
          }"
    )
```

```
setInterval(checkifrunning, 1000)'

```
signifie qu'il se déclenche si l'opérationest plus longue qu’une seconde (unité milliseconde)

---

Insérer dans l’UI à l’endroit où on veut que l’indicateur s’affiche :


```r
tags$div(id = "loading", 
				tags$script('$("#loading").hide()'))
```

## 7.2 Arrêter proprement l’application

Pour des raisons de sécurité, on ne peut pas fermer la fenêtre/onglet du navigateur par programme.

En cas de fermeture de la fenêtre/onglet du navigateur par l’utilisateur, pour arrêter l’application, mettre dans le `server` :


```r
session$onSessionEnded(stopApp)
```

## 7.3 Scinder le code source

Il est fortement conseillé de :

* couper le source en au moins autant de fichiers que de pages dashboard
* isoler la définition du menu des fonctions utilitaires (par exemple `menu.R` et `fonctions.R`)
* isoler le lanceur `app.R` du reste. Lorsque l’application contient beaucoup de pages, il est possible de charger tout le contenu d’un répertoire
* organiser les fichiers dans des sous-répertoires

---

Organisation des répertoires à la racine du répertoire de l'application 

![](www/img/59 Scinder source 1.PNG)

Le répertoire `data` (ou `donnees`) contient toutes les tables de données fournies dans l’application (de préférence en format RDS).

![](www/img/59 Scinder source 2.PNG)

Pour appeler un fichier de code R :


```r
source(file = "init.R", echo = FALSE, encoding = "UTF-8")
source("utils/fonctions.R")
source("pages/onglet_accueil.R")
```

Le code sera compilé dans l'ordre dans lequel les fichiers sont appelés.

* **Contenu des fichiers scindés pour l'UI :**

    - *dans le fichier général de l'UI*


```r
dashboardBody(
  tabItems(
    tabItem(tabName = "onglet_accueil_ui",            
            source(file("pages/onglet_accueil_ui.R"), local = TRUE)$value
    )
```

    - *dans le fichier "pages/onglet_accueil_ui.R"*


```r
fluidRow(
...
)
```

* Contenu des fichiers scindés pour le `server` :

    - dans le fichier général du server


```r
shinyServer(function(input, output) {
  onglet_accueil(input, output)
  onglet_parametrage(input, output)
  ...
})
```

    - dans le fichier "pages/onglet_accueil_server.R"


```r
onglet_accueil <- function(input, output) {
...
}
```

---

**Répertoire www :**

* fichiers image
* fichiers CSS (feuilles de style) 
* fichiers javascripts...

appel de ces fichiers dans l’UI :


```r
tags$img(height = 100, width = 100,
				src = "mon_image.png")
theme = "feuille_de_style.css"
includeCSS("feuille_de_style.css")
```

---

Il est possible d'appeler toutes les fichiers de code automatiquement


```r
source("init.R", echo=FALSE, encoding="UTF-8")
purrr ::walk(
  list.files("pages/",full.names=TRUE),
  function (x) source(x, echo=FALSE, encoding="UTF-8"))
source("all.R", echo=FALSE, encoding="UTF-8")
source("dashboard.R", echo=FALSE, encoding="UTF-8")
shinyApp(ui, server, options=list(launch.browser=TRUE))
```
init.R 		chargement des packages, définition des paramètres globaux
all.R		ensemble des fonctions utilitaires communes aux pages

Avec quelques conventions, il est facile de charger toutes les définitions d’interface « ui » ou tous les serveurs en une seule boucle.

**Exemple :** chaque source de page contient les fonctions :


```r
page$<nom de la page>$ui
```
Dans l’ui, le chargement des définitions d’interface :


```r
do.call(tabItems,
        purrr ::map(names(page), 
                    function(x) tabItem(x,page[[x]]$ui()))
)

page$<nom de la page>$server 
```
Dans le server, le chargement des serveurs :


```r
purrr::walk(names(page),
     		   function(x) page[[x]]$server(input,output,session))
```

## 7.4 Créer un raccourci pour lancer l’application

1- Créer un programme R avec :

* les librairies
* le `runApp` en renseignant le chemin de l’application

![](www/img/59 runApp.PNG)

## 7.5 Créer un exécutable pour lancer l'application

1- Créer un programme R (ici, runApp ExerciceFinal.R) avec :

* au minimum, la librairie `shiny`
* le `runApp()` en renseignant le chemin du script à exécuter (ici, FormationDashboard ExerciceFinal.R).
 
![](www/img/59 runApp 2.PNG)
 
2- Créer un fichier "text" (avec notePad++ par exemple) :

* Ajouter les chemins pointant vers :
    - la version de R utilisée (ici, R-3.3.3),
    - le script contenant la fonction runApp() (ici, run App ExerciceFinal.R)
        - pause signifie que la fenêtre restera ouverte, ce qui est utile pour debugger votre appli si quelque chose se passe mal,
        - exit (à la place de pause) pour que la fenêtre se ferme automatiquement avec l'appli.
* l'enregistrer et le nommer (ici, Executer ExerciceFinal)
* modifier son extension en : .bat

![](www/img/59 runApp 3.PNG)
